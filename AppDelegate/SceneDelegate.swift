//
//  SceneDelegate.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 15/12/2020.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        self.setRootViewController()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    static func getInstatnce() -> SceneDelegate {
        let scene = UIApplication.shared.connectedScenes.first
        return scene?.delegate as! SceneDelegate
    }
    
    func setRootViewController() {
        
        let nav = RootViewController.instantiateFromStoryboard()
        self.window?.rootViewController = nav
        let vc = SplashViewController.instantiateFromStoryboard()
        nav.pushViewController(vc, animated: true)
        
    }
    
    func navigateTOInitialViewController() {
        
        //        if UserManager.isUserLogin() {
        
                    let controller = SideMenuRootViewController.instantiateFromStoryboard()
                    controller.leftViewPresentationStyle = .slideAbove
                    self.window?.rootViewController = controller
        
        //        } else {
        //
//        let nav = RootViewController.instantiateFromStoryboard()
//        self.window?.rootViewController = nav
//        //            let vc = TutorialOneViewController.instantiateFromStoryboard()
//        let vc = LoginViewController.instantiateFromStoryboard()
//        nav.pushViewController(vc, animated: true)
        //
        //        }
        
    }
    
}

