//
//  AppDelegate.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 15/12/2020.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyBnz1Z14WDDlz95wcl2Kd2goy3ssUEkw3E")
        
        IQKeyboardManager.shared.enable = true
        self.setRootViewController()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {

    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("Enter Foreground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("Become Active")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func setRootViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let nav = RootViewController.instantiateFromStoryboard()
        self.window?.rootViewController = nav
        let vc = SplashViewController.instantiateFromStoryboard()
        nav.pushViewController(vc, animated: true)
        
    }
    
    func navigateTOInitialViewController() {
        
//        self.window = UIWindow(frame: UIScreen.main.bounds)
        
//        if UserManager.isUserLogin() {
//
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            self.window?.rootViewController = controller
//
//        } else {

//            let nav = RootViewController.instantiateFromStoryboard()
//            self.window?.rootViewController = nav
//            let vc = LoginViewController.instantiateFromStoryboard()
//            nav.pushViewController(vc, animated: true)

//        }
        
    }
    
}

