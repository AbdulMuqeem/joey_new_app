//
//  MDTextField.swift
//  Sample App
//
//  Created by Muqeem's Macbook on 11/06/2021.
//

import UIKit
import MaterialComponents.MaterialTextFields

class MDTextField: UIView {

    @IBOutlet weak var textField:MDCTextField!
    var textController:MDCTextInputControllerOutlined!
    var view: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupTextField(placeholder : String , keyboardType : UIKeyboardType) {
        
        //TextFeild
        self.textField.placeholder = placeholder
        self.textField.keyboardType = keyboardType
        self.textField.clearButton.isHidden = true
        self.textField.contentHorizontalAlignment = .center
        
        let font = UIFont(name: "Poppins-Regular", size: GISTUtility.convertToRatio(13.0))!
        self.textField.font = font
        
        //Leading Image
//        let bundle = Bundle(for: TextFieldKitchenSinkSwiftExample.self)
//        let leadingViewImage = UIImage(named: "help", in: bundle, compatibleWith: nil)
//        self.textField.leadingViewMode = .always
//        self.textField.leadingView = UIImageView(image:leadingViewImage)
//        self.textField.translatesAutoresizingMaskIntoConstraints = false
        
//        self.textField.leadingViewMode = .always
//        self.textField.leadingView = UIImageView(image: UIImage(named: "help"))
//        self.textField.translatesAutoresizingMaskIntoConstraints = false

        //Container
        self.textController = MDCTextInputControllerOutlined(textInput: self.textField)
        self.textController.textInput?.textColor = .black
        self.textController.borderRadius = 4
        self.textController.activeColor = Theme_Orange_Color
        self.textController.floatingPlaceholderActiveColor = Theme_Orange_Color
        self.textController.normalColor = .gray
        self.textController.borderFillColor = .white
//        self.textController.textInsets(UIEdgeInsets(top: 20, left: 10, bottom: -10, right: 10))
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    
    func xibSetup(){
        
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
    }
    
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
        
    }
    
}
