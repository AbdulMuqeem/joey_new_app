//
//  DocumentCell.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 24/02/2021.
//

import UIKit

class DocumentCell: UITableViewCell {

    @IBOutlet weak var lblDocumentType:UILabel!
    @IBOutlet weak var lblExpiryData:UILabel!
    @IBOutlet weak var imgDoccument:UIImageView!
    @IBOutlet weak var mainView: UIView!
    
    var indexpath:IndexPath?
    var delegate:UploadDocumentDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func approvedAction(_ sender : UIButton) {
        
    }
    
    @IBAction func uploadAction(_ sender : UIButton) {
        self.delegate!.uploadDocumetAction(indexpath: self.indexpath!)
    }

}
