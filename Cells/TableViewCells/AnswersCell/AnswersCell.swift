//
//  AnswersCell.swift
//  JoeyCo
//
//  Created by MacBook Air on 18/05/2021.
//

import UIKit

class AnswersCell: UITableViewCell {

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var lblCorrectAnswer: UILabel!
    @IBOutlet weak var lblCorrectAnswerDetail: UILabel!
    
    @IBOutlet weak var imageBackgorundView: UIView!
    @IBOutlet weak var answerImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
