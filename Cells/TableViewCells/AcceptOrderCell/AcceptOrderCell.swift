//
//  AcceptOrderCell.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 18/01/2021.
//

import UIKit

class AcceptOrderCell: UITableViewCell {
    
    var delegate:AcceptOrderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func checkListAction(_ sender : UIButton) {
        self.delegate?.checkListAction()
    }
    
    @IBAction func mapAction(_ sender : UIButton) {
        self.delegate?.mapAction()
    }
    
    @IBAction func callAction(_ sender : UIButton) {
        self.delegate?.callAction()
    }
    
    @IBAction func delayAction(_ sender : UIButton) {
        self.delegate?.delayAction()
    }
    
    @IBAction func returnAction(_ sender : UIButton) {
        self.delegate?.returnAction()
    }
    
}
