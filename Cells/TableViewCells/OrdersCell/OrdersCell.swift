//
//  OrdersCell.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 29/06/2021.
//

import UIKit

class OrdersCell: UITableViewCell {

    @IBOutlet weak var newOrdersView:UIView!
    @IBOutlet weak var acceptOrdersView:UIView!
    
    var delegate:OrderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.newOrdersView.cornerRadius = self.newOrdersView.frame.height * 0.45
        self.newOrdersView.layoutIfNeeded()
        
        self.acceptOrdersView.cornerRadius = self.acceptOrdersView.frame.height * 0.45
        self.acceptOrdersView.layoutIfNeeded()
        
    }

    @IBAction func acceptOrderAction(_ sender : UIButton) {
        self.delegate?.acceptAction()
    }

    
}
