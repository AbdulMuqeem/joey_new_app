//
//  ItinaryOrderCell.swift
//  Joey
//
//  Created by Muqeem's Macbook on 01/02/2021.
//

import UIKit

class ItinaryOrderCell: UITableViewCell {

    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblStatus:UILabel!
    @IBOutlet weak var upperView:UIView!
    @IBOutlet weak var lowerView:UIView!
        
    var delegate:ItinaryOrderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func mapAction(_ sender : UIButton) {
        self.delegate?.mapAction()
    }
    
    @IBAction func callAction(_ sender : UIButton) {
        self.delegate?.callAction()
    }
    
    @IBAction func infoAction(_ sender : UIButton) {
        self.delegate?.infoAction()
    }
    
    @IBAction func pickupAction(_ sender : UIButton) {
        self.delegate?.pickupScanAction()
    }
    
    @IBAction func dropoffAction(_ sender : UIButton) {
        self.delegate?.dropoffScanAction()
    }
    
    @IBAction func delayAction(_ sender : UIButton) {
        self.delegate?.delayAction()
    }
    
}
