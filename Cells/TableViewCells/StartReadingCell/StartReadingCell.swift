//
//  StartReadingCell.swift
//  JoeyCo
//
//  Created by MacBook Air on 17/05/2021.
//

import UIKit

class StartReadingCell: UITableViewCell {

    @IBOutlet weak var tutorialImageView: UIImageView!
    
    @IBOutlet weak var lblTutorialDetail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
