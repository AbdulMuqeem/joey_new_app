//
//  WatchTutorialCell.swift
//  JoeyCo
//
//  Created by MacBook Air on 16/05/2021.
//

import UIKit

class WatchTutorialCell: UITableViewCell {

    @IBOutlet weak var tutorialImageView: UIImageView!
    
    @IBOutlet weak var lblTutorialDetail: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
