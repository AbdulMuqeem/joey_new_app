//
//  HomeVehicleTypeCell.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 13/01/2021.
//

import UIKit

class HomeVehicleTypeCell: UICollectionViewCell {

    @IBOutlet weak var imgVehicle:UIImageView!
    @IBOutlet weak var lblVehicleName:UILabel!
    @IBOutlet weak var bgView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
