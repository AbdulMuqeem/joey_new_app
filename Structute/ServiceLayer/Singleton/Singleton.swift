//
//  Singleton.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
//import CoreData

private let singleton = Singleton()

class Singleton {
    
    var CurrentUser:User? = UserManager.getUserObjectFromUserDefaults()
    
    class var sharedInstance: Singleton {
        return singleton
    }
}

private let header = Header()

class Header {
    
    var  CurrentHeader: [String: String]? = UserManager.getHeader()
    
    class var sharedInstance: Header {
        return header
    }
}
