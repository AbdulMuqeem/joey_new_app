//
//  UserManager.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import UIKit
import Foundation

class UserManager {

    var sessionDeviceToken: String?

    static func saveUserObjectToUserDefaults(userResult: User){
        let archiveData = NSKeyedArchiver.archivedData(withRootObject: userResult)
        UserDefaults.standard.set(archiveData, forKey: User_data_userDefault)
    }

    static func removeUserObjectFromUserDefaults() {
        UserDefaults.standard.set(nil, forKey: User_data_userDefault)
        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
        UserDefaults.standard.synchronize()
    }

    static func getUserObjectFromUserDefaults() -> User? {

        let userData:NSData?  = UserDefaults.standard.value(forKey: User_data_userDefault) as? NSData

        if userData != nil {
            let userDataObj = NSKeyedUnarchiver.unarchiveObject(with: userData! as Data) as? User
            return userDataObj
        }
        else{
            return nil
        }
    }

    static func isUserLogin() -> Bool {

        if  self.getUserObjectFromUserDefaults() != nil {
            return true
        }
        else{
            return false
        }

    }
    
    static func getHeader() -> [String: String] {
        var header: [String: String] = [String: String]()
        if UserDefaults.standard.object(forKey: token_userDefault) != nil {
            let token = UserDefaults.standard.object(forKey: token_userDefault)
            header = ["Authorization":token as! String]
        }
        return header
    }

}

