//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSObject , NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let isSenderDocInfoAvailable = "isSenderDocInfoAvailable"
        static let middleName = "middleName"
        static let mobile = "mobile"
        static let isSenderAddInfoAvailable = "isSenderAddInfoAvailable"
        static let branchDefaultCashAccountId = "branchDefaultCashAccountId"
        static let lastName = "lastName"
        static let token = "token"
        static let userId = "userId"
        static let customerStateId = "customerStateId"
        static let username = "username"
        static let customerId = "customerId"
        static let branchDefaultCashAccountName = "branchDefaultCashAccountName"
        static let branchCountryId = "branchCountryId"
        static let branchDefaultCashAccountTypeId = "branchDefaultCashAccountTypeId"
        static let availableZeroCommRemits = "availableZeroCommRemits"
        static let firstName = "firstName"
        static let countryId = "countryId"
    }
    
    // MARK: Properties
    public var isSenderDocInfoAvailable: Int?
    public var middleName: String?
    public var mobile: String?
    public var isSenderAddInfoAvailable: Int?
    public var branchDefaultCashAccountId: Int?
    public var lastName: String?
    public var token: String?
    public var userId: Int?
    public var customerStateId: Int?
    public var username: String?
    public var customerId: Int?
    public var branchDefaultCashAccountName: String?
    public var branchCountryId: Int?
    public var branchDefaultCashAccountTypeId: Int?
    public var availableZeroCommRemits: Int?
    public var firstName: String?
    // public var encrypt: Bool? = false
    public var countryId: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        isSenderDocInfoAvailable = json[SerializationKeys.isSenderDocInfoAvailable].int
        middleName = json[SerializationKeys.middleName].string
        mobile = json[SerializationKeys.mobile].string
        isSenderAddInfoAvailable = json[SerializationKeys.isSenderAddInfoAvailable].int
        branchDefaultCashAccountId = json[SerializationKeys.branchDefaultCashAccountId].int
        lastName = json[SerializationKeys.lastName].string
        token = json[SerializationKeys.token].string
        userId = json[SerializationKeys.userId].int
        customerStateId = json[SerializationKeys.customerStateId].int
        username = json[SerializationKeys.username].string
        customerId = json[SerializationKeys.customerId].int
        branchDefaultCashAccountName = json[SerializationKeys.branchDefaultCashAccountName].string
        branchCountryId = json[SerializationKeys.branchCountryId].int
        branchDefaultCashAccountTypeId = json[SerializationKeys.branchDefaultCashAccountTypeId].int
        availableZeroCommRemits = json[SerializationKeys.availableZeroCommRemits].int
        firstName = json[SerializationKeys.firstName].string
        // encrypt = json[SerializationKeys.encrypt].boolValue
        countryId = json[SerializationKeys.countryId].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = isSenderDocInfoAvailable { dictionary[SerializationKeys.isSenderDocInfoAvailable] = value }
        if let value = middleName { dictionary[SerializationKeys.middleName] = value }
        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
        if let value = isSenderAddInfoAvailable { dictionary[SerializationKeys.isSenderAddInfoAvailable] = value }
        if let value = branchDefaultCashAccountId { dictionary[SerializationKeys.branchDefaultCashAccountId] = value }
        if let value = lastName { dictionary[SerializationKeys.lastName] = value }
        if let value = token { dictionary[SerializationKeys.token] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = customerStateId { dictionary[SerializationKeys.customerStateId] = value }
        if let value = username { dictionary[SerializationKeys.username] = value }
        if let value = customerId { dictionary[SerializationKeys.customerId] = value }
        if let value = branchDefaultCashAccountName { dictionary[SerializationKeys.branchDefaultCashAccountName] = value }
        if let value = branchCountryId { dictionary[SerializationKeys.branchCountryId] = value }
        if let value = branchDefaultCashAccountTypeId { dictionary[SerializationKeys.branchDefaultCashAccountTypeId] = value }
        if let value = availableZeroCommRemits { dictionary[SerializationKeys.availableZeroCommRemits] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }
        //  dictionary[SerializationKeys.encrypt] = encrypt
        if let value = countryId { dictionary[SerializationKeys.countryId] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.isSenderDocInfoAvailable = aDecoder.decodeObject(forKey: SerializationKeys.isSenderDocInfoAvailable) as? Int
        self.middleName = aDecoder.decodeObject(forKey: SerializationKeys.middleName) as? String
        self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
        self.isSenderAddInfoAvailable = aDecoder.decodeObject(forKey: SerializationKeys.isSenderAddInfoAvailable) as? Int
        self.branchDefaultCashAccountId = aDecoder.decodeObject(forKey: SerializationKeys.branchDefaultCashAccountId) as? Int
        self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
        self.token = aDecoder.decodeObject(forKey: SerializationKeys.token) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.customerStateId = aDecoder.decodeObject(forKey: SerializationKeys.customerStateId) as? Int
        self.username = aDecoder.decodeObject(forKey: SerializationKeys.username) as? String
        self.customerId = aDecoder.decodeObject(forKey: SerializationKeys.customerId) as? Int
        self.branchDefaultCashAccountName = aDecoder.decodeObject(forKey: SerializationKeys.branchDefaultCashAccountName) as? String
        self.branchCountryId = aDecoder.decodeObject(forKey: SerializationKeys.branchCountryId) as? Int
        self.branchDefaultCashAccountTypeId = aDecoder.decodeObject(forKey: SerializationKeys.branchDefaultCashAccountTypeId) as? Int
        self.availableZeroCommRemits = aDecoder.decodeObject(forKey: SerializationKeys.availableZeroCommRemits) as? Int
        self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
        //   self.encrypt = aDecoder.decodeBool(forKey: SerializationKeys.encrypt)
        self.countryId = aDecoder.decodeObject(forKey: SerializationKeys.countryId) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(isSenderDocInfoAvailable, forKey: SerializationKeys.isSenderDocInfoAvailable)
        aCoder.encode(middleName, forKey: SerializationKeys.middleName)
        aCoder.encode(mobile, forKey: SerializationKeys.mobile)
        aCoder.encode(isSenderAddInfoAvailable, forKey: SerializationKeys.isSenderAddInfoAvailable)
        aCoder.encode(branchDefaultCashAccountId, forKey: SerializationKeys.branchDefaultCashAccountId)
        aCoder.encode(lastName, forKey: SerializationKeys.lastName)
        aCoder.encode(token, forKey: SerializationKeys.token)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(customerStateId, forKey: SerializationKeys.customerStateId)
        aCoder.encode(username, forKey: SerializationKeys.username)
        aCoder.encode(customerId, forKey: SerializationKeys.customerId)
        aCoder.encode(branchDefaultCashAccountName, forKey: SerializationKeys.branchDefaultCashAccountName)
        aCoder.encode(branchCountryId, forKey: SerializationKeys.branchCountryId)
        aCoder.encode(branchDefaultCashAccountTypeId, forKey: SerializationKeys.branchDefaultCashAccountTypeId)
        aCoder.encode(availableZeroCommRemits, forKey: SerializationKeys.availableZeroCommRemits)
        aCoder.encode(firstName, forKey: SerializationKeys.firstName)
        //   aCoder.encode(encrypt, forKey: SerializationKeys.encrypt)
        aCoder.encode(countryId, forKey: SerializationKeys.countryId)
    }
    
}
