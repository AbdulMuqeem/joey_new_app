//
//  Enums.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation

enum BackButton: String {
    case LIGHT
    case DARK
}
