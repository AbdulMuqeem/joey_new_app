//
//  Constants.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit
    
public let THEME_COLOR: UInt = 0xe46d29

public let BASE_URL = "http://167.114.24.251:96/" // Development
//public let BASE_URL = "http://167.114.24.251:3099/" // Production

public let Google_Api_Key = "AIzaSyCBjDrjC7fCW7QaNxl18sFY5tfAuBnZW-0"

public let BACK_IMAGE: UIImage = UIImage(named:"back_icon")!
public let MENU_IMAGE: UIImage = UIImage(named:"menu_icon")!

public let WARNING:UIColor = UIColor.init(named: "Warning_Color")!
public let FAILURE:UIColor = UIColor.init(named: "Failure_Color")!
public let SUCCESS:UIColor = UIColor.init(named: "Success_Color")!

public let Theme_Orange_Color:UIColor = UIColor.init(named: "Theme_Orange_Color")!
public let Theme_Green_Color:UIColor = UIColor.init(named: "Theme_Green_Color")!
public let Dark_Purple_Color:UIColor = UIColor.init(named: "Dark_Purple_Color")!
public let Medium_Purple_Color:UIColor = UIColor.init(named: "Medium_Purple_Color")!
public let Light_Purple_Color:UIColor = UIColor.init(named: "Light_Purple_Color")!
public let Gray_Shadow_Color:UIColor = UIColor.init(named: "Gray_Shadow_Color")!
public let Placeholder_Color:UIColor = UIColor.init(named: "Placeholder_Color")!

// Alert
public let SUCCESS_IMAGE: UIImage = UIImage(named:"success")!
public let FAILURE_IMAGE: UIImage = UIImage(named:"error")!

let USERUPDATED = "UserUpdated"

public var REGISTER_USER_ID:Int? = 0
public var ORDER_NUMBER:String? = ""
public var PAYMENT_UPDATE:Bool? = false
public var User_Document_ID = [Int]()

let BANKELIVERYMETHODUPDATED = "BankDeliveryMethodUpdated"
let LOGIN_TOKEN = "Token"

var isAirTimeCheckRates = false

let SESSIONEXPIRED = "SessionExpired"
var isSessionExpired = false

var MOBILEDELIVERYOPERATORTYPE:String = ""
var MOBILEDELIVERYTYPE:String = ""
var AIRTIMEDELIVERYTYPE:String = ""
var BANKDELIVERYTYPE:String = ""

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"

