//
//  Protocols.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshDelegate {
    func StartRefresh()
}

protocol AlertViewDelegate {
    func okAction()
}

protocol AlertViewDelegateAction {
    func okButtonAction()
    func cancelAction()
}

protocol UploadDocumentDelegate {
    func uploadDocumetAction(indexpath:IndexPath)
}

protocol ItinaryOrderDelegate {
    func mapAction()
    func callAction()
    func infoAction()
    func pickupScanAction()
    func dropoffScanAction()
    func delayAction()
}

protocol OrderDelegate {
    func acceptAction()
}

protocol AcceptOrderDelegate {
    func checkListAction()
    func mapAction()
    func callAction()
    func delayAction()
    func returnAction()
}

