//
//  UINavigationControllerExtension.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    func TransparentNavigationBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.backgroundColor = UIColor.clear
        setNavigationBarHidden(false, animated: true)
    }
    
    func ThemedNavigationBar() {
        //  let image = UIImage(named: "topbar")!
        //self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        self.navigationBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        self.navigationBar.isTranslucent = false
    }
    
    func ChangeNavigationBarColor(color: UIColor) {
        self.navigationBar.barTintColor = color
        self.navigationBar.isTranslucent = false
    }
    
    func HideNavigationBar() {
        self.isNavigationBarHidden = true
    }
    
    func ShowNavigationBar() {
        self.isNavigationBarHidden = false
    }
    
    func hideNavigationBarLine() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    func ChangeTitleColor(color: UIColor) {
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
    }
    
    func ChangeTitleFont() {
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                  NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: GISTUtility.convertToRatio(17.0))!]
    }
    
}

