//
//  UIViewControllerExtension.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import BRYXBanner
import Darwin

extension UIViewController {
    
    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    func CornerView(view : UIView) {
        view.layer.cornerRadius = view.frame.height * 0.45
    }
    
    func CornerButton(view : UIButton) {
        view.layer.cornerRadius = view.frame.height * 0.45
    }
    
    func setDecimalValues(number:Double , precision:Double) -> Double {
        let numberOfPlaces = precision
        let multiplier = pow(10.0, numberOfPlaces)
        let rounded = round(number * multiplier) / multiplier
        return rounded
    }

    func getDecodedValue(url:String) -> String {
        if let range = url.range(of: "q=") {
            let code = url[range.upperBound...].trimmingCharacters(in: .whitespaces)
            return code
        }
        return ""
    }
    
    func getCurrentTime() -> String {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "E, MMM dd yyyy"
        let dateString = df.string(from: date)
        return dateString
    }
    
    func getCurrentYear() -> String {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy"
        let dateString = df.string(from: date)
        return dateString
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateString = df.string(from: date)
        return dateString
    }

    
    func generateRandomStringWithLength(length: Int) -> String {
        let randomString: NSMutableString = NSMutableString(capacity: length)
        let letters: NSMutableString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var i: Int = 0
        
        while i < length {
            let randomIndex: Int = Int(arc4random_uniform(UInt32(letters.length)))
            randomString.append("\(Character( UnicodeScalar( letters.character(at: randomIndex))!))")
            i += 1
        }
        return String(randomString)
    }
        
    func DarkStatusBar() {
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    
    func LightStatusBar() {
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .lightContent
        } else {
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
}

extension UIViewController : NVActivityIndicatorViewable {
    
    //MARK:- Native Phone Call
    
    func callNumber(phoneNumber:String) {

      if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {

        let application:UIApplication = UIApplication.shared
        if (application.canOpenURL(phoneCallURL)) {
            application.open(phoneCallURL, options: [:], completionHandler: nil)
        }
      }
    }
    
    //MARK:- Loader 
    
    func startLoading(message:String){
        let size = CGSize(width: 80, height:80)
        startAnimating(size, message: message , type:.ballTrianglePath)
    }
    
    func stopLoading(){
        stopAnimating()
    }
    
    //MARK:- Banner
    func showBanner(title:String , subTitle:String , type:UIColor) {
        
        let banner = Banner(title: title , subtitle: subTitle , backgroundColor: type)
        banner.dismissesOnTap = true
        banner.show(duration: 3.0)
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK:- Alert Controller
    
    // Alert Popup  Ok
    func Alert(title : String,message : String) {
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: NSLocalizedString("OK", comment: "") , style: UIAlertAction.Style.default) { (action) -> Void in
            // do something after completation
        }
        
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // Alert Popup  Ok Action
    func ShowAlertAction(title : String,message : String, OkActionHandler:@escaping (_ message:Any?)->Void){
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: NSLocalizedString("Ok", comment: "") , style: UIAlertAction.Style.default) { (action) -> Void in
            // do something after completation
            OkActionHandler("pressed ok")
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // Alert Popup  Ok Cancel Action
    func ShowAlert(title : String,message : String, OkActionHandler:@escaping (_ message:Any?)->Void){
        
        let alert = UIAlertController(title: title as String, message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        let doneAction = UIAlertAction(title: NSLocalizedString("Yes", comment: "") , style: UIAlertAction.Style.default) { (action) -> Void in
            // do something after completation
            OkActionHandler("pressed ok")
        }
        
        alert.addAction(cancelAction)
        alert.addAction(doneAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func PlaceNavImage(image: UIImage) {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        // let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:image)
        self.navigationItem.titleView = imageView
        
    }
    
    func PlaceNavImageBackButton(image: UIImage, selector: Selector , titleImage: UIImage) {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        // let logo = UIImage(named: "logo.png")
        let imageView = UIImageView(image:titleImage)
        self.navigationItem.titleView = imageView
        
        var btnLeft: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(image, for: .normal)
        btnLeft?.addTarget(self, action: selector, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    //MARK:- Left Button Code
    
    func PlaceLeftButton(image: UIImage, selector: Selector) {
        
        var btnLeft: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(image, for: .normal)
        btnLeft?.addTarget(self, action: selector, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
    }
    
    //MARK:- Left Button Title Code
    
    func PlaceLeftButton(selectorForLeftText : Selector , leftTitle : String)  {
        
        var btnLeft: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setTitle(leftTitle, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeftText, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
    }
    
    //MARK:- Right Button Title Code
    
    func PlaceRightButton(selectorForRightText : Selector , rightTitle : String)  {
        
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnRight = UIButton(type: .custom)
        btnRight?.setTitle(rightTitle, for: .normal)
        btnRight?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    
    //MARK:- Right Button Code
    
    func PlaceRightButton(image: UIImage, selector: Selector) {
        
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnRight = UIButton(type: .custom)
        btnRight?.setImage(image, for: .normal)
        btnRight?.addTarget(self, action: selector, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    
    
    //MARK:-  Left Button & right Button Code
    
    func PlaceNavigationButtons(selectorForLeftText : Selector, leftTitle:String ,  selectorForRightText : Selector , rightTitle:String) {
        
        var btnLeft: UIButton?
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setTitle(leftTitle, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeftText, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
        btnRight = UIButton(type: .custom)
        btnRight?.setTitle(rightTitle, for: .normal)
        btnRight?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    func PlaceNavigationButtons(selectorForLeft : Selector, leftImage:UIImage ,  selectorForRight : Selector , rightImage:UIImage) {
        
        var btnLeft: UIButton?
        var btnRight: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        btnRight = UIButton(type: .custom)
        btnRight?.setImage(rightImage, for: .normal)
        btnRight?.addTarget(self, action: selectorForRight, for: .touchUpInside)
        btnRight?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let rightBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRight!)
        self.navigationItem.rightBarButtonItems = [ rightBarButton ]
        
    }
    
    
    //MARK:-  Left Buttons with right button title Code
    
    func PlaceLeftButtons(selectorForLeft : Selector, leftImage : UIImage , selectorForRightText : Selector , rightTitle : String) {
        
        var btnLeft: UIButton?
        var btnRightText: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLeft = UIButton(type: .custom)
        btnLeft?.setImage(leftImage, for: .normal)
        btnLeft?.addTarget(self, action: selectorForLeft, for: .touchUpInside)
        btnLeft?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let leftBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnLeft!)
        self.navigationItem.leftBarButtonItems = [ leftBarButton ]
        
        btnRightText = UIButton(type: .custom)
        btnRightText?.setTitle(rightTitle, for: .normal)
        btnRightText?.setTitleColor(WARNING, for: .normal)
        btnRightText?.titleLabel!.font = UIFont(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        
        btnRightText?.addTarget(self, action: selectorForRightText, for: .touchUpInside)
        btnRightText?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        
        let rightTextBarButton: UIBarButtonItem = UIBarButtonItem(customView: btnRightText!)
        self.navigationItem.rightBarButtonItems = [ rightTextBarButton ]
        
    }
    
    func PlaceNavigationBackButtons(selectorForLeftLogo : Selector, leftLogoImage : UIImage , selectorForLeftText : Selector , leftTitle : String) {
        
        var btnLogo: UIButton?
        var btnLeftText: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLogo = UIButton(type: .custom)
        btnLogo?.setImage(leftLogoImage, for: .normal)
        btnLogo?.addTarget(self, action: selectorForLeftLogo, for: .touchUpInside)
        btnLogo?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let leftBtnLogo: UIBarButtonItem = UIBarButtonItem(customView: btnLogo!)
        
        btnLeftText = UIButton(type: .custom)
        btnLeftText?.setTitle(leftTitle, for: .normal)
        btnLeftText?.addTarget(self, action: selectorForLeftText, for: .touchUpInside)
        btnLeftText?.frame = CGRect(x: 0, y: 0 , width: 200, height: 25)
        let leftBtnText: UIBarButtonItem = UIBarButtonItem(customView: btnLeftText!)
        
        self.navigationItem.leftBarButtonItems = [ leftBtnLogo , leftBtnText ]
    }
    
    func PlaceNavigationHomeButtons(selectorForLeftLogo : Selector, leftLogoImage : UIImage ,selectorForRightSearch : Selector, rightSearchImage : UIImage , selectorForRightCart : Selector, rightCartImage : UIImage ) {
        
        var btnLogo: UIButton?
        
        var btnSearch: UIButton?
        var btnCart: UIButton?
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.rightBarButtonItem = nil
        
        btnLogo = UIButton(type: .custom)
        btnLogo?.setImage(leftLogoImage, for: .normal)
        btnLogo?.addTarget(self, action: selectorForLeftLogo, for: .touchUpInside)
        btnLogo?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let leftBtnLogo: UIBarButtonItem = UIBarButtonItem(customView: btnLogo!)
        
        self.navigationItem.leftBarButtonItems = [ leftBtnLogo  ]
        
        btnSearch = UIButton(type: .custom)
        btnSearch?.setImage(rightSearchImage, for: .normal)
        btnSearch?.addTarget(self, action: selectorForRightSearch, for: .touchUpInside)
        btnSearch?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let RightBtnSearch: UIBarButtonItem = UIBarButtonItem(customView: btnSearch!)
        
        btnCart = UIButton(type: .custom)
        btnCart?.setImage(rightCartImage, for: .normal)
        btnCart?.addTarget(self, action: selectorForRightCart, for: .touchUpInside)
        btnCart?.frame = CGRect(x: 0, y: 0 , width: 25, height: 25)
        let RightBtnCart: UIBarButtonItem = UIBarButtonItem(customView: btnCart!)
        
        self.navigationItem.rightBarButtonItems = [ RightBtnSearch , RightBtnCart ]
    }
    
}



