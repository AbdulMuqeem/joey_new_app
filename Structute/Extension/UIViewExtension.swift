//
//  UIViewExtension.swift
//  Paysii
//
//  Created by Abdul Muqeem on 10/04/2020.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
        layer.shadowRadius = 1.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeIn(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeOut(withDuration duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
}

extension UIImageView {
    
    func setImageFromUrl(urlStr:String) {
        
        let tempStr = urlStr.replacingOccurrences(of: "\\", with: "/")
        let tempStr1 = tempStr.replacingOccurrences(of: " ", with: "%20")
        
        if AppHelper.isValidURL(str: tempStr1) {
            let url = URL(string: tempStr1)!
            
            var kingf = self.kf
            kingf.indicatorType = .activity
            kingf.setImage(with: url)
            
        }
        
    }
    
    //    func fixOrientation(img: UIImage) -> UIImage {
    //        if (img.imageOrientation == .up) {
    //            return img
    //        }
    //
    //        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
    //        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
    //        img.draw(in: rect)
    //
    //        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
    //        UIGraphicsEndImageContext()
    //
    //        return normalizedImage
    //    }
    
}

//extension UIImageView {
//
//    func setImageFromUrl url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
//        contentMode = mode
//        URLSession.shared.dataTask(with: url) { data, response, error in
//            guard
//                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
//                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
//                let data = data, error == nil,
//                let image = UIImage(data: data)
//                else { return }
//            DispatchQueue.main.async() {
//                self.image = image
//            }
//            }.resume()
//    }
//
//    func setImageFromUrl link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
//        guard let url = URL(string: link) else { return }
//        setImageFromUrl(urlStr: url, contentMode: mode)
//    }
//}

extension UIButton {
    
    func setImageURL(urlStr:String) {
        
        let tempStr = urlStr.replacingOccurrences(of: "\\", with: "/")
        let tempStr1 = tempStr.replacingOccurrences(of: " ", with: "%20")
        
        if AppHelper.isValidURL(str: tempStr1) {
            
            let url = URL(string: tempStr1)!
            self.kf.setImage(with: url, for: .normal)
            
        }
        
    }
}
