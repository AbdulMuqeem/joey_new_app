//
//  UpdateProfileViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit

extension UpdateProfileViewController {
    
    func initialSetup() {
        
        var placeholder = ""
        
        if self.typeId == 1 {
            self.title = "Update Name"
            self.lblTitle.text = "Update Your Name"
            self.lblSummary.text = "Your name make it easy for us"
            placeholder = "Full name"
            self.txtField.keyboardType = .alphabet
        }
        else if self.typeId == 2 {
            self.title = "Update Phone"
            self.lblTitle.text = "Update Your Phone Number"
            self.lblSummary.text = "We will send a code to your mobile number to verify your account"
            placeholder = "Mobile Number"
            self.txtField.keyboardType = .numberPad
        }
        else if self.typeId == 3 {
            self.title = "Update Email"
            self.lblTitle.text = "Update Your Email"
            self.lblSummary.text = "Receive info about new updates & awesome promos in your inbox"
            placeholder = "Email Address"
            self.txtField.keyboardType = .emailAddress
        }
        
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
            
        self.txtField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
    }
    
}

class UpdateProfileViewController: UIViewController {

    class func instantiateFromStoryboard() -> UpdateProfileViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! UpdateProfileViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblSummary:UILabel!
    @IBOutlet weak var txtField:UITextField!
    
    var typeId:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

}
