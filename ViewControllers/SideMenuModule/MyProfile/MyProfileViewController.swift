//
//  MyProfileViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 28/12/2020.
//

import UIKit

extension MyProfileViewController {
    
    func initialSetup() {
        
        self.title = "My Account"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class MyProfileViewController: UIViewController {

    class func instantiateFromStoryboard() -> MyProfileViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyProfileViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func personalAction(_ sender : UIButton) {
        let vc = PersonalDetailViewController.instantiateFromStoryboard()
        vc.isProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func vehicleAction(_ sender : UIButton) {
        let vc = VehicleInformationViewController.instantiateFromStoryboard()
        vc.isProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func workAction(_ sender : UIButton) {
        let vc = WorkPreferencesViewController.instantiateFromStoryboard()
        vc.isProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func additionalAction(_ sender : UIButton) {
        let vc = AdditionalInformationViewController.instantiateFromStoryboard()
        vc.isProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

//    @IBAction func genderAction(_ sender : UIButton) {
//        self.view.endEditing(true)
//        self.genderDropDown.dataSource = self.genderArray
//        self.genderDropDown.show()
//    }
//
//    @IBAction func dateOfBirthAction(_ sender : UIButton) {
//        self.view.endEditing(true)
//
//        ActionSheetDatePicker.show(withTitle: "Date Of Birth", datePickerMode: .date , selectedDate: Date(), minimumDate: nil , maximumDate: Date() , doneBlock: { (picker, date, origin) in
//
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
//            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
//            self.lblDob.text = newDate
//
//        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
//    }
//
//    @IBAction func displayNameAction(_ sender : UIButton) {
//        self.view.endEditing(true)
//        self.displayNameDropDown.dataSource = self.displayNameArray
//        self.displayNameDropDown.show()
//    }
//
//    @IBAction func languageAction(_ sender : UIButton) {
//        self.view.endEditing(true)
//        self.languageDropDown.dataSource = self.languageArray
//        self.languageDropDown.show()
//    }
    
}

