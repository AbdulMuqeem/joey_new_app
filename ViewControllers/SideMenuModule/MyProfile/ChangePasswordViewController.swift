//
//  ChangePasswordViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit

extension ChangePasswordViewController : UITextFieldDelegate {
    
    func initialSetup() {
        
        self.title = "Change Password"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.txtOldPassword.delegate = self
        self.txtNewPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        
        self.txtOldPassword.attributedPlaceholder = NSAttributedString(string: "Enter old password", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtNewPassword.attributedPlaceholder = NSAttributedString(string: "Enter new password", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Enter confirm new password", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.oldPasswordLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 2 {
            self.newPasswordLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 3 {
            self.confirmPasswordLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.oldPasswordLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 2 {
            self.newPasswordLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 3 {
            self.confirmPasswordLineView.backgroundColor = UIColor.darkGray
        }
        
        return true
    }
    
}


class ChangePasswordViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ChangePasswordViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ChangePasswordViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var txtOldPassword:UITextField!
    @IBOutlet weak var txtNewPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    @IBOutlet weak var oldPasswordLineView:UIView!
    @IBOutlet weak var newPasswordLineView:UIView!
    @IBOutlet weak var confirmPasswordLineView:UIView!
    
    @IBOutlet weak var imgOldEye:UIImageView!
    @IBOutlet weak var imgNewEye:UIImageView!
    @IBOutlet weak var imgConfirmEye:UIImageView!
    
    var showOldPassword = true
    var showNewPassword = true
    var showConfirmPassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showOldPasswordAction(_ sender : UIButton) {
        
        if self.showOldPassword == true {
            self.imgOldEye.image = UIImage(named: "password_show")
            self.txtOldPassword.isSecureTextEntry = false
        }
        else {
            self.imgOldEye.image = UIImage(named: "password_hide")
            self.txtOldPassword.isSecureTextEntry = true
        }
        
        self.showOldPassword = !self.showOldPassword
        
    }
    
    @IBAction func showNewPasswordAction(_ sender : UIButton) {
        
        if self.showNewPassword == true {
            self.imgNewEye.image = UIImage(named: "password_show")
            self.txtNewPassword.isSecureTextEntry = false
        }
        else {
            self.imgNewEye.image = UIImage(named: "password_hide")
            self.txtNewPassword.isSecureTextEntry = true
        }
        
        self.showNewPassword = !self.showNewPassword
        
    }

    @IBAction func showConfirmPasswordAction(_ sender : UIButton) {
        
        if self.showConfirmPassword == true {
            self.imgConfirmEye.image = UIImage(named: "password_show")
            self.txtConfirmPassword.isSecureTextEntry = false
        }
        else {
            self.imgConfirmEye.image = UIImage(named: "password_hide")
            self.txtConfirmPassword.isSecureTextEntry = true
        }
        
        self.showConfirmPassword = !self.showConfirmPassword
        
    }

}
