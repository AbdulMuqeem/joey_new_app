//
//  ProfileVehicleInformationViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 09/06/2021.
//

import UIKit

class ProfileVehicleInformationViewController: UIViewController {
    
    @IBOutlet weak var bicycleView: UIView!
    @IBOutlet weak var suvView: UIView!
    @IBOutlet weak var carView: UIView!
    @IBOutlet weak var truckView: UIView!
    @IBOutlet weak var scooterView: UIView!
    @IBOutlet weak var vanView: UIView!
    
    @IBOutlet weak var lblBicycle: UILabel!
    @IBOutlet weak var lblSUV: UILabel!
    @IBOutlet weak var lblCar: UILabel!
    @IBOutlet weak var lblTruck: UILabel!
    @IBOutlet weak var lblScooter: UILabel!
    @IBOutlet weak var lblVAN: UILabel!
    
    @IBOutlet weak var bicycleImgVw: UIImageView!
    @IBOutlet weak var suvImgVw: UIImageView!
    @IBOutlet weak var carImgVw: UIImageView!
    @IBOutlet weak var truckImgVw: UIImageView!
    @IBOutlet weak var scooterImgVw: UIImageView!
    @IBOutlet weak var vanImgVw: UIImageView!
    
    @IBOutlet weak var makeBorderView: UIView!
    @IBOutlet weak var colorBorderView: UIView!
    @IBOutlet weak var modelBorderView: UIView!
    @IBOutlet weak var licensePlateBorderView: UIView!
    
    @IBOutlet weak var txtMake: UITextField!
    @IBOutlet weak var txtColor: UITextField!
    @IBOutlet weak var txtModel: UITextField!
    @IBOutlet weak var txtLicensePlate: UITextField!
    
    class func instantiateFromStoryboard() -> ProfileVehicleInformationViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfileVehicleInformationViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    func initialSetup() {
        
        self.title = "Vehicle Details"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        
        self.txtMake.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtColor.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtModel.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtLicensePlate.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.bicycleView.layer.cornerRadius = 10
        self.bicycleView.clipsToBounds = true
        
        self.suvView.layer.cornerRadius = 10
        self.bicycleView.clipsToBounds = true
        
        self.carView.layer.cornerRadius = 10
        self.carView.clipsToBounds = true
        
        self.truckView.layer.cornerRadius = 10
        self.truckView.clipsToBounds = true
        
        self.scooterView.layer.cornerRadius = 10
        self.scooterView.clipsToBounds = true
        
        self.vanView.layer.cornerRadius = 10
        self.vanView.clipsToBounds = true
        
        self.makeBorderView.layer.cornerRadius = 10
        self.makeBorderView.clipsToBounds = true
        
        self.colorBorderView.layer.cornerRadius = 10
        self.colorBorderView.clipsToBounds = true
        
        self.modelBorderView.layer.cornerRadius = 10
        self.modelBorderView.clipsToBounds = true
        
        self.licensePlateBorderView.layer.cornerRadius = 10
        self.licensePlateBorderView.clipsToBounds = true
        
        self.txtMake.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtColor.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtModel.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtLicensePlate.attributedPlaceholder = NSAttributedString(string: "", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
