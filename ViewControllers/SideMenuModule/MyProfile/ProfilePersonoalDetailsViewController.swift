//
//  ProfilePersonoalDetailsViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 09/06/2021.
//

import UIKit

class ProfilePersonoalDetailsViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ProfilePersonoalDetailsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfilePersonoalDetailsViewController
    }
    
    //txtField outlets
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPhoneNumber:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtUnitNumber:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    func initialSetup() {
        
        self.title = "Personal Details"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.txtFirstName.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtLastName.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtAddress.attributedPlaceholder = NSAttributedString(string: "Address", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtUnitNumber.attributedPlaceholder = NSAttributedString(string: "Unit Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])

    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
