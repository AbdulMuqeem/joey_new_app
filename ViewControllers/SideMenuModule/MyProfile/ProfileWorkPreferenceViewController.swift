//
//  ProfileWorkPreferenceViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 09/06/2021.
//

import UIKit
import ActionSheetPicker_3_0

class ProfileWorkPreferenceViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ProfileWorkPreferenceViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfileWorkPreferenceViewController
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var fullTimeView: UIView!
    @IBOutlet weak var partTimeView: UIView!
    @IBOutlet weak var seasonalView: UIView!
    @IBOutlet weak var casualView: UIView!
    
    @IBOutlet weak var lblFullTime: UILabel!
    @IBOutlet weak var lblPartTime: UILabel!
    @IBOutlet weak var lblSeasonal: UILabel!
    @IBOutlet weak var lblCasual: UILabel!
    
    @IBOutlet weak var bestTimeView: UIView!
    @IBOutlet weak var preferredZoneView: UIView!
    
    @IBOutlet weak var lblDayTime: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblPreferredZone: UILabel!
    
    @IBOutlet weak var groceryView: UIView!
    @IBOutlet weak var ecommerceView: UIView!
    
    @IBOutlet weak var lblGrocery: UILabel!
    @IBOutlet weak var lblEcommerce: UILabel!
    
    @IBOutlet weak var shadowView: UIView!
    
    //BottomView Outlets
    @IBOutlet weak var bottomView:UIView!
    
    @IBOutlet weak var calgaryView: UIView!
    @IBOutlet weak var ottawaView: UIView!
    @IBOutlet weak var jeddaView: UIView!
    @IBOutlet weak var downtownTorontoView: UIView!
    @IBOutlet weak var montrealView: UIView!
    @IBOutlet weak var helifaxView: UIView!
    
    @IBOutlet weak var lblCalgary: UILabel!
    @IBOutlet weak var lblOttawa: UILabel!
    @IBOutlet weak var lblJedda: UILabel!
    @IBOutlet weak var lblDowntownToronto: UILabel!
    @IBOutlet weak var lblMontreal: UILabel!
    @IBOutlet weak var lblHelifax: UILabel!
    
    var isProfile:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    func initialSetup() {
        
        self.title = "Work Preference"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.fullTimeView.layer.cornerRadius = 10
        self.fullTimeView.clipsToBounds = true
        
        self.partTimeView.layer.cornerRadius = 10
        self.partTimeView.clipsToBounds = true
        
        self.seasonalView.layer.cornerRadius = 10
        self.seasonalView.clipsToBounds = true
        
        self.casualView.layer.cornerRadius = 10
        self.casualView.clipsToBounds = true
        
        self.bestTimeView.layer.cornerRadius = 10
        self.bestTimeView.clipsToBounds = true
        
        self.preferredZoneView.layer.cornerRadius = 10
        self.preferredZoneView.clipsToBounds = true
        
        self.groceryView.layer.cornerRadius = 10
        self.groceryView.clipsToBounds = true
        
        self.ecommerceView.layer.cornerRadius = 10
        self.ecommerceView.clipsToBounds = true
        
        //BottomView UI
        self.calgaryView.layer.cornerRadius = 10
        self.calgaryView.clipsToBounds = true
        
        self.ottawaView.layer.cornerRadius = 10
        self.ottawaView.clipsToBounds = true
        
        self.jeddaView.layer.cornerRadius = 10
        self.jeddaView.clipsToBounds = true
        
        self.downtownTorontoView.layer.cornerRadius = 10
        self.downtownTorontoView.clipsToBounds = true
        
        self.montrealView.layer.cornerRadius = 10
        self.montrealView.clipsToBounds = true
        
        self.helifaxView.layer.cornerRadius = 10
        self.helifaxView.clipsToBounds = true
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
        self.scrollView.isScrollEnabled = true
    }
    
    @IBAction func btnActionFullTime(_ sender: Any) {
        
    }
    
    @IBAction func btnActionPartTime(_ sender: Any) {
        
    }
    
    @IBAction func btnActionSeasonal(_ sender: Any) {
        
    }
    
    @IBAction func btnActionCasual(_ sender: Any) {
        
    }
    
    @IBAction func btnActionBestTime(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Select Time", datePickerMode: .date , selectedDate: Date(), minimumDate: nil , maximumDate: Date() , doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblDayTime.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    
    @IBAction func btnActionPreferredZone(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
        //        self.scrollView.isScrollEnabled = false
    }
    
    @IBAction func btnActionGrocery(_ sender: Any) {
        
    }
    
    @IBAction func btnActionEcommerce(_ sender: Any) {
        
    }
    
    @IBAction func btnActionSaveAndContinue(_ sender: Any) {
        let vc = AdditionalInformationViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnActionPrevious(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //BottomView Actions
    @IBAction func btnActionCalgary(_ sender: Any) {
        
    }
    
    @IBAction func btnActionOttawa(_ sender: Any) {
        
    }
    
    @IBAction func btnActionJedda(_ sender: Any) {
        
    }
    
    @IBAction func btnActionDowntownToronto(_ sender: Any) {
        
    }
    
    @IBAction func btnActionMontreal(_ sender: Any) {
        
    }
    
    @IBAction func btnActionHelifax(_ sender: Any) {
        
    }
    
}

