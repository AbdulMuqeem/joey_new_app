//
//  ProfileAdditionalInformationViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 09/06/2021.
//

import UIKit

class ProfileAdditionalInformationViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ProfileAdditionalInformationViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfileAdditionalInformationViewController
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var txtInstitutionNumber:UITextField!
    @IBOutlet weak var txtBranchNumber:UITextField!
    @IBOutlet weak var txtAccountNumber:UITextField!
    @IBOutlet weak var txtHSTNumber:UITextField!
    @IBOutlet weak var txtCardDepositNumber:UITextField!
    
    //txtLine outlets
    @IBOutlet weak var institutionNumberView:UIView!
    @IBOutlet weak var branchNumberView:UIView!
    @IBOutlet weak var accountNumberView:UIView!
    @IBOutlet weak var hstNumberView:UIView!
    @IBOutlet weak var cardDepositNumberView:UIView!
    
    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    //BottomView Outlets
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var lblTwitter: UILabel!
    @IBOutlet weak var lblInstagram: UILabel!
    @IBOutlet weak var lblLinkedin: UILabel!
    
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var twitterView: UIView!
    @IBOutlet weak var instagramView: UIView!
    @IBOutlet weak var linkedinView: UIView!
    
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var txtOther:UITextField!
    
    var isProfile:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    func initialSetup() {
        
        self.title = "Additional Information"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.feedbackView.layer.cornerRadius = 10
        self.feedbackView.clipsToBounds = true
        
        self.txtInstitutionNumber.attributedPlaceholder = NSAttributedString(string: "Institution Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtBranchNumber.attributedPlaceholder = NSAttributedString(string: "Branch Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtAccountNumber.attributedPlaceholder = NSAttributedString(string: "Account Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtHSTNumber.attributedPlaceholder = NSAttributedString(string: "HST Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        self.txtCardDepositNumber.attributedPlaceholder = NSAttributedString(string: "RBC Card Deposit Number", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
        self.scrollView.isScrollEnabled = true
    }
    
    
    @IBAction func btnActionFeedback(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
        self.scrollView.isScrollEnabled = false
    }
    
}
