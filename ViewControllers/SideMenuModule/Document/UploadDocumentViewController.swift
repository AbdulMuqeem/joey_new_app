//
//  UploadDocumentViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 25/02/2021.
//

import UIKit
import ActionSheetPicker_3_0

extension UploadDocumentViewController {
    
    func initialSetup() {
        
        self.title = "Upload Document"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class UploadDocumentViewController: UIViewController {

    class func instantiateFromStoryboard() -> UploadDocumentViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! UploadDocumentViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var lblExpiryDate:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func expiryDateAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Expiry Date", datePickerMode: .date , selectedDate: Date(), minimumDate: Date() , maximumDate: nil , doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblExpiryDate.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }

    
}
