//
//  DocumentListViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 24/02/2021.
//

import UIKit

extension DocumentListViewController {
    
    func initialSetup() {
        
        self.title = "Verify Documents"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class DocumentListViewController: UIViewController {

    class func instantiateFromStoryboard() -> DocumentListViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DocumentListViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var tblView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Register cell
        let nib = UINib(nibName: "DocumentCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "DocumentCell")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }

}

extension DocumentListViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell:DocumentCell =  tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath) as! DocumentCell

        if indexPath.row == 0 {
            cell.lblDocumentType.text = "Permit"
            cell.lblExpiryData.text = "Expiry: 23-02-2022"
        }
        else if indexPath.row == 1 {
            cell.lblDocumentType.text = "License"
            cell.lblExpiryData.text = "Expiry: 02-03-2023"
        }
        
        cell.indexpath = indexPath
        cell.delegate = self
        
        return cell
        
    }
    
}

extension DocumentListViewController : UploadDocumentDelegate {
    
    func uploadDocumetAction(indexpath: IndexPath) {
        
        let vc = UploadDocumentViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
