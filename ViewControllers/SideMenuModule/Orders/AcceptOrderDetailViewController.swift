//
//  AcceptOrderDetailViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 15/06/2021.
//

import UIKit
import GoogleMaps

extension AcceptOrderDetailViewController {
    
    func initialSetup() {
        
        self.title = "Active Order"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.mapView.delegate = self
        //        self.didAccesToLocation()
        //        self.determineMyCurrentLocation()
        
        self.getMap(33.7295, 73.0372)
        
    }
    
}

extension AcceptOrderDetailViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
        self.ShowAlert(title: "Access Permission", message: "Goldfin App needs to access your location to show your current location") { (sender) in
            
            let url = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
    
}


class AcceptOrderDetailViewController: UIViewController , UIGestureRecognizerDelegate {
    
    class func instantiateFromStoryboard() -> AcceptOrderDetailViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AcceptOrderDetailViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var reportDelaytblView:UITableView!
    
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var swipeView:UIView!
    @IBOutlet weak var bottomViewHeightConstraints:BaseLayoutConstraint!
    var isViewHide:Bool! = false
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var openMapDialogueView:UIView!
    @IBOutlet weak var delayDialogueView:UIView!
    
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register cell
        let nib = UINib(nibName: "AcceptOrderCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "AcceptOrderCell")
        
        let nib1 = UINib(nibName: "ReportDelayCell", bundle: nil)
        self.reportDelaytblView.register(nib1, forCellReuseIdentifier: "ReportDelayCell")
        
        //Swipe Gesture
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.swipeView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.swipeView.addGestureRecognizer(swipeDown)
        
        //Tap Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        tapGesture.delegate = self
        self.swipeView.addGestureRecognizer(tapGesture)
        
        self.shadowView.isHidden = true
        self.openMapDialogueView.isHidden = true
        self.delayDialogueView.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.openMapDialogueView.clipsToBounds = true
        self.openMapDialogueView.layer.cornerRadius = 60
        self.openMapDialogueView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner] //Bottom
        
        self.delayDialogueView.clipsToBounds = true
        self.delayDialogueView.layer.cornerRadius = 60
        self.delayDialogueView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner] //top

        self.view.layoutIfNeeded()
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .up {
            self.bottomViewHeightConstraints.constant = GISTUtility.convertToRatio(620.0)
            self.isViewHide = false
        }
        else if gesture.direction == .down {
            self.bottomViewHeightConstraints.constant = GISTUtility.convertToRatio(170.0)
            self.isViewHide = true
        }
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @objc func handleTapGesture(gesture: UITapGestureRecognizer) -> Void {
        
        if self.isViewHide == true {
            self.bottomViewHeightConstraints.constant = GISTUtility.convertToRatio(620.0)
            self.isViewHide = false
        }
        else {
            self.bottomViewHeightConstraints.constant = GISTUtility.convertToRatio(170.0)
            self.isViewHide = true
        }
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if touch?.view != self.openMapDialogueView {
            self.shadowView.isHidden = true
            self.openMapDialogueView.isHidden = true
        }
        
        if touch?.view != self.delayDialogueView {
            self.shadowView.isHidden = true
            self.delayDialogueView.isHidden = true
        }

    }

    
}

//MARK:- TableView Delegate & DataSource
extension AcceptOrderDetailViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblView {
            return 3
        }
        else {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblView {
            return GISTUtility.convertToRatio(270.0)
        }
        else {
            return GISTUtility.convertToRatio(60.0)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblView {
            let cell:AcceptOrderCell = tableView.dequeueReusableCell(withIdentifier: "AcceptOrderCell" , for: indexPath) as! AcceptOrderCell
            
            cell.delegate = self
            return cell
        }
        else {
            let cell:ReportDelayCell = tableView.dequeueReusableCell(withIdentifier: "ReportDelayCell" , for: indexPath) as! ReportDelayCell
                    
            if indexPath.row == 0 {
                cell.lblTitle.text = "Joey at pickup location"
            }
            else if indexPath.row == 1 {
                cell.lblTitle.text = "Delay at pickup location"
            }
            else if indexPath.row == 2 {
                cell.lblTitle.text = "Joey incident"
            }
            else if indexPath.row == 3 {
                cell.lblTitle.text = "Delivery to hub for re-delivery"
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != self.tblView {
            self.shadowView.isHidden = true
            self.delayDialogueView.isHidden = true
        }
    }

}

//MARK:- Delegate Actions
extension AcceptOrderDetailViewController : AcceptOrderDelegate {
    
    func checkListAction() {
        self.showBanner(title: "Alert", subTitle: "CheckList dialogue will be open in beta build", type: WARNING)
    }
    
    func mapAction() {
        self.shadowView.isHidden = false
        self.openMapDialogueView.isHidden = false
        
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
    }
    
    func callAction() {
        let vc = CallViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
        
    func delayAction() {
        
        self.shadowView.isHidden = false
        self.delayDialogueView.isHidden = false
        
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }

    }
    
    func returnAction() {
        
        self.shadowView.isHidden = false
        self.delayDialogueView.isHidden = false
        
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }

    }
    
    @IBAction func cancelReportDelayAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.shadowView.isHidden = true
        self.delayDialogueView.isHidden = true
    }
    
}
