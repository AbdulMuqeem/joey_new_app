//
//  OrdersViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit
import GoogleMaps

extension OrdersViewController {
    
    func initialSetup() {
        
        self.title = "Grocerry Orders"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.mapView.delegate = self
        //        self.didAccesToLocation()
        //        self.determineMyCurrentLocation()
        
        self.getMap(33.7295, 73.0372)
        
    }
    
}

extension OrdersViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
        self.ShowAlert(title: "Access Permission", message: "Goldfin App needs to access your location to show your current location") { (sender) in
            
            let url = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
    
}


class OrdersViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> OrdersViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! OrdersViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var tblView:UITableView!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var bottomViewHeight:NSLayoutConstraint!
    
    @IBOutlet weak var newOrdersView:UIView!
    @IBOutlet weak var activeOrdersView:UIView!
    
    @IBOutlet weak var lblNewOrder:UILabel!
    @IBOutlet weak var lblActiveOrder:UILabel!
    
    @IBOutlet weak var lblNewOrdersCount:UILabel!
    @IBOutlet weak var lblActiveOrdersCount:UILabel!
    
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    var ordersType:String = "New"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register cell
//        let nib = UINib(nibName: "OrdersCell", bundle: nil)
//        self.collectionView.register(nib, forCellWithReuseIdentifier: "OrdersCell")
        
        //Register cell
        let xib = UINib(nibName: "OrdersCell", bundle: nil)
        self.tblView.register(xib, forCellReuseIdentifier: "OrdersCell")
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
//        self.bottomViewHeight.constant = 0.0
        
        self.tblView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.roundCorners(corners: [.topRight , .topLeft], radius: 60)
        self.view.layoutIfNeeded()
        
    }
            
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func newOrdersAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.newOrdersView.backgroundColor = Theme_Orange_Color
        self.newOrdersView.BorderColor = Theme_Orange_Color
        self.newOrdersView.shadowColor = Theme_Orange_Color
        self.lblNewOrder.textColor = .white
        
        self.activeOrdersView.backgroundColor = .white
        self.activeOrdersView.BorderColor = Gray_Shadow_Color
        self.activeOrdersView.shadowColor = Gray_Shadow_Color
        self.lblActiveOrder.textColor = Theme_Green_Color
                
        self.ordersType = "New"
        self.tblView.reloadData()
        
    }
    
    @IBAction func activeOrdersAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.activeOrdersView.backgroundColor = Theme_Orange_Color
        self.activeOrdersView.BorderColor = Theme_Orange_Color
        self.activeOrdersView.shadowColor = Theme_Orange_Color
        self.lblActiveOrder.textColor = .white
        
        self.newOrdersView.backgroundColor = .white
        self.newOrdersView.BorderColor = Gray_Shadow_Color
        self.newOrdersView.shadowColor = Gray_Shadow_Color
        self.lblNewOrder.textColor = Theme_Green_Color
        
        self.bottomView.isHidden = true
        
        self.ordersType = "Accept"
        self.tblView.reloadData()
        
    }
    
    
}

//MARK:- CollectionView Delegate & Datasource
//extension OrdersViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
//
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        if self.ordersType == "New" {
//            return 6
//        }
//
//        return 6
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let height = GISTUtility.convertToRatio(150.0)
//        let screenWidth =  collectionView.bounds.size.width //- 20
//        return CGSize(width: screenWidth/1 , height: height)
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell:OrdersCell = collectionView.dequeueReusableCell(withReuseIdentifier:"OrdersCell" , for: indexPath) as! OrdersCell
//
//        if self.ordersType == "New" {
//            cell.acceptOrdersView.isHidden = false
//        }
//        else {
//            cell.acceptOrdersView.isHidden = true
//        }
//
//        cell.delegate = self
//        return cell
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
////                let obj = self.MyCourseListArray![indexPath.row]
//
//        if self.ordersType != "New" {
//            let vc = AcceptOrderDetailViewController.instantiateFromStoryboard()
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//
//    }
//}

//MARK:- TableView Delegate & Datasource
extension OrdersViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.ordersType == "New" {
            return 5
        }
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(150.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell:OrdersCell =  tableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath) as! OrdersCell
        
        if self.ordersType == "New" {
            cell.acceptOrdersView.isHidden = false
        }
        else {
            cell.acceptOrdersView.isHidden = true
        }
        
        cell.delegate = self
                
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.ordersType != "New" {
            let vc = AcceptOrderDetailViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

//MARK:- AcceptOrder Functionality
extension OrdersViewController : OrderDelegate {
    
    func acceptAction() {
        
//        self.bottomViewHeight.constant = 300.0
        self.shadowView.isHidden = false
        self.bottomView.isHidden = false
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func cancelAction(_ sender : UIButton) {
        
//        self.bottomViewHeight.constant = 0.0
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func okAction(_ sender : UIButton) {
        
//        self.bottomViewHeight.constant = 0.0
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
        
        self.showBanner(title: "Success!", subTitle: "Order has been accepted", type: SUCCESS)
    }
    
}
