//
//  TermsViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 01/01/2021.
//

import UIKit

extension TermsViewController {
    
    func initialSetup() {
        
        self.title = "Terms & Conditions"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class TermsViewController: UIViewController {

    class func instantiateFromStoryboard() -> TermsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TermsViewController
    }
    
    @IBOutlet weak var txtView:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.loadData()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadData() {
        let text = "By using the JoeyCo website (“JoeyCo Site“) and its related JoeyCo electronic applications and services ('Services'), you hereby agree to these legally-binding terms and conditions (the 'Terms and Conditions'). Do not access or use the JoeyCo Site if you are unwilling or unable to be bound by them.\n\nThese Terms and Conditions constitute the entire agreement between you ('you' or 'User') and JoeyCo Inc. and its affiliates, subsidiaries, partners, officers, directors, agents and employees (collectively, 'JoeyCo') pertaining to your use of the JoeyCo Site.\n\nFor information on how user information is collected, used and disclosed by JoeyCo in connection with your use of the JoeyCo Site, please review our Privacy Policy.\n\nJoeyCo reserves the right to suspend or terminate your access to the JoeyCo Site, at any time for convenience, or for any other reason, including without limitation if JoeyCo has determined in its sole discretion that the use of the JoeyCo Site was in breach of these Terms and Conditions."
        self.txtView.text = text
    }

}
