//
//  HelpViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit

extension HelpViewController {
    
    func initialSetup() {
        
        self.title = "Help"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class HelpViewController: UIViewController {

    class func instantiateFromStoryboard() -> HelpViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HelpViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func aboutAction(_ sender : UIButton) {
        let vc = AboutViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func faqAction(_ sender : UIButton) {
        let vc = FAQViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func contactAction(_ sender : UIButton) {
        let vc = ContactViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyAction(_ sender : UIButton) {
        let vc = PrivacyPolicyViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tutorialsAction(_ sender : UIButton) {
        let vc = SelectTutorialTypeViewController.instantiateFromStoryboard()
        vc.isHelp = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        let vc = TermsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
