//
//  FAQViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 01/01/2021.
//

import UIKit

extension FAQViewController {
    
    func initialSetup() {
        
        self.title = "FAQ"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.layoutIfNeeded()
        
        self.amazonView.backgroundColor = Placeholder_Color
        self.wallmartView.backgroundColor = .white
        self.joeycoView.backgroundColor = .white
        
        self.lblAmazon.textColor = .white
        self.lblWallmart.textColor = .darkGray
        self.lblJoeyCo.textColor = .darkGray
        
        self.amazonView.cornerRadius = self.amazonView.frame.height * 0.45
        self.wallmartView.cornerRadius = self.wallmartView.frame.height * 0.45
        self.joeycoView.cornerRadius = self.joeycoView.frame.height * 0.45
           
        self.lblFirstTitle.text = "What cities is JoeyCo available in?"
        self.lblFirstDescription.text = "JoeyCo is currently available in Toronto but will be expanding to new cities very soon."
        
        self.lblSecondTitle.text = "What can I have delivered with JoeyCo?"
        self.lblSecondDescription.text = "You name it, you got it. From groceries to dry cleaning and everything in between, if it’s in a store or restaurant in your city, we can deliver it."
        
        self.lblThirdTitle.text = "Who makes the deliveries?"
        self.lblThirdDescription.text = "Delivery Agents, known as “Joey’s” are enthusiastic members of your neighbourhood. When your delivery is accepted, you are immediately introduced to your Joey through their profile, delivery records and their ratings."
        
        self.lblFourthTitle.text = "What modes of transport do the Joeys use?"
        self.lblFourthDescription.text = "You name it, you got it. From groceries to dry cleaning and everything in between, if it’s in a store or restaurant in your city, we can deliver it."
        
        self.lblFifthTitle.text = "How much does it cost?"
        self.lblFifthDescription.text = "Delivery fees starting from $5.00 and are determined by an algorithm that accounts for the distance, time required and effort involved in the delivery. For more details, read about our dynamic pricing model."
        
    }
    
}

class FAQViewController: UIViewController {

    class func instantiateFromStoryboard() -> FAQViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FAQViewController
    }
    
    @IBOutlet weak var amazonView:UIView!
    @IBOutlet weak var wallmartView:UIView!
    @IBOutlet weak var joeycoView:UIView!
    
    @IBOutlet weak var lblAmazon:UILabel!
    @IBOutlet weak var lblWallmart:UILabel!
    @IBOutlet weak var lblJoeyCo:UILabel!
    
    @IBOutlet weak var FirstImage:UIImageView!
    @IBOutlet weak var FirstDescriptionView:UIView!
    @IBOutlet weak var lblFirstTitle:UILabel!
    @IBOutlet weak var lblFirstDescription:UILabel!
    
    @IBOutlet weak var SecondImage:UIImageView!
    @IBOutlet weak var SecondDescriptionView:UIView!
    @IBOutlet weak var lblSecondTitle:UILabel!
    @IBOutlet weak var lblSecondDescription:UILabel!
    
    @IBOutlet weak var ThirdImage:UIImageView!
    @IBOutlet weak var ThirdDescriptionView:UIView!
    @IBOutlet weak var lblThirdTitle:UILabel!
    @IBOutlet weak var lblThirdDescription:UILabel!
    
    @IBOutlet weak var FourthImage:UIImageView!
    @IBOutlet weak var FourthDescriptionView:UIView!
    @IBOutlet weak var lblFourthTitle:UILabel!
    @IBOutlet weak var lblFourthDescription:UILabel!
    
    @IBOutlet weak var FifthImage:UIImageView!
    @IBOutlet weak var FifthDescriptionView:UIView!
    @IBOutlet weak var lblFifthTitle:UILabel!
    @IBOutlet weak var lblFifthDescription:UILabel!

    var showFirstView = true
    var showSecondView = true
    var showThirdView = true
    var showFourthView = true
    var showFiveView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func amazonAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.amazonView.backgroundColor = Placeholder_Color
        self.wallmartView.backgroundColor = .white
        self.joeycoView.backgroundColor = .white
        
        self.lblAmazon.textColor = .white
        self.lblWallmart.textColor = .darkGray
        self.lblJoeyCo.textColor = .darkGray
        
    }
    
    @IBAction func wallmartAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.amazonView.backgroundColor = .white
        self.wallmartView.backgroundColor = Placeholder_Color
        self.joeycoView.backgroundColor = .white
        
        self.lblAmazon.textColor = .darkGray
        self.lblWallmart.textColor = .white
        self.lblJoeyCo.textColor = .darkGray
        
    }
    
    @IBAction func joeyCoAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.amazonView.backgroundColor = .white
        self.wallmartView.backgroundColor = .white
        self.joeycoView.backgroundColor = Placeholder_Color
        
        self.lblAmazon.textColor = .darkGray
        self.lblWallmart.textColor = .darkGray
        self.lblJoeyCo.textColor = .white
        
    }

    @IBAction func firstAction(_ sender : UIButton) {
        
        if showFirstView == true {
            if #available(iOS 13.0, *) {
                self.FirstImage.image = UIImage(named: "up_arrow")?.withTintColor(Theme_Green_Color)
            } else {
                self.FirstImage.image = UIImage(named: "up_arrow")
            }
            self.FirstDescriptionView.isHidden = false
        }
        else {
            self.FirstImage.image = UIImage(named: "down_arrow")
            self.FirstDescriptionView.isHidden = true
        }
        
        showFirstView = !showFirstView
    
    }
    
    @IBAction func secondAction(_ sender : UIButton) {

        if showSecondView == true {
            if #available(iOS 13.0, *) {
                self.SecondImage.image = UIImage(named: "up_arrow")?.withTintColor(Theme_Green_Color)
            } else {
                self.SecondImage.image = UIImage(named: "up_arrow")
            }
            self.SecondDescriptionView.isHidden = false
        }
        else {
            self.SecondImage.image = UIImage(named: "down_arrow")
            self.SecondDescriptionView.isHidden = true
        }

        showSecondView = !showSecondView

    }
    
    @IBAction func thirdAction(_ sender : UIButton) {

        if showThirdView == true {
            if #available(iOS 13.0, *) {
                self.ThirdImage.image = UIImage(named: "up_arrow")?.withTintColor(Theme_Green_Color)
            } else {
                self.ThirdImage.image = UIImage(named: "up_arrow")
            }
            self.ThirdDescriptionView.isHidden = false
        }
        else {
            self.ThirdImage.image = UIImage(named: "down_arrow")
            self.ThirdDescriptionView.isHidden = true
        }

        showThirdView = !showThirdView

    }
    
    @IBAction func fourthAction(_ sender : UIButton) {

        if showFourthView == true {
            if #available(iOS 13.0, *) {
                self.FourthImage.image = UIImage(named: "up_arrow")?.withTintColor(Theme_Green_Color)
            } else {
                self.ThirdImage.image = UIImage(named: "up_arrow")
            }
            self.FourthDescriptionView.isHidden = false
        }
        else {
            self.FourthImage.image = UIImage(named: "down_arrow")
            self.FourthDescriptionView.isHidden = true
        }

        showFourthView = !showFourthView

    }
    
    @IBAction func fifthAction(_ sender : UIButton) {

        if showFiveView == true {
            if #available(iOS 13.0, *) {
                self.FifthImage.image = UIImage(named: "up_arrow")?.withTintColor(Theme_Green_Color)
            } else {
                self.ThirdImage.image = UIImage(named: "up_arrow")
            }
            self.FifthDescriptionView.isHidden = false
        }
        else {
            self.FifthImage.image = UIImage(named: "down_arrow")
            self.FifthDescriptionView.isHidden = true
        }

        showFiveView = !showFiveView

    }

    
}
