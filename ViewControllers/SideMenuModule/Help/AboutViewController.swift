//
//  AboutViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 01/01/2021.
//

import UIKit

extension AboutViewController {
    
    func initialSetup() {
        
        self.title = "About"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class AboutViewController: UIViewController {

    class func instantiateFromStoryboard() -> AboutViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AboutViewController
    }
    
    @IBOutlet weak var txtView:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.loadData()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadData() {
        let text = "JoeyCo is always with you – at home, in the office, or even on the boardwalk – making it great for getting things done on the fly. But even though we make great last minute saves, JoeyCo also lets you schedule your tasks ahead of time, making it a great service for anyone who likes to plan ahead.\n\nGetting everything you need delivered on time.Supporting the environment and encouraging eco-friendly transportation.\n\nJoin our ever-growing family of over 900 Joeys.Make your own schedule, enjoy a steady income and have fun!"
        self.txtView.text = text
    }
}
