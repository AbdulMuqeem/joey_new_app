//
//  TutorialsViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 01/01/2021.
//

import UIKit

extension TutorialsViewController {
    
    func initialSetup() {
        
        self.title = "Tutorials"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class TutorialsViewController: UIViewController {

    class func instantiateFromStoryboard() -> TutorialsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TutorialsViewController
    }
    
    var isHelp:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }


}
