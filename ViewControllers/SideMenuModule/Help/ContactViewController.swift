//
//  ContactViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 01/01/2021.
//

import UIKit

extension ContactViewController : UITextFieldDelegate , UITextViewDelegate {
    
    func initialSetup() {
        
        self.title = "Contact Us"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
               
        self.txtOrderId.attributedPlaceholder = NSAttributedString(string: "Enter Order Id", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Regular", size: GISTUtility.convertToRatio(13.0))!
        ])
        
        self.txtDescription.placeholder = " Enter Description"
        
        let count = self.ComplainListArray.count
        let height = 45
        let calculatedHeight = ((height * count) + 20) / 2
        self.collectionViewHeight.constant = CGFloat(calculatedHeight)
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.OrderLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.OrderLineView.backgroundColor = UIColor.lightGray
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if self.txtDescription.text.count == 0 {
            self.txtDescription.placeholder = " Enter Description"
        }
        if self.txtDescription.text.count > 0 {
            self.txtDescription.placeholder = ""
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.txtDescription.layer.borderColor = UIColor.init(rgb: THEME_COLOR).cgColor
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.txtDescription.layer.borderColor = UIColor.lightGray.cgColor
        return true
    }
    
}

class ContactViewController: UIViewController {

    class func instantiateFromStoryboard() -> ContactViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ContactViewController
    }
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var collectionViewHeight:BaseLayoutConstraint!
    
    @IBOutlet weak var txtOrderId:UITextField!
    @IBOutlet weak var txtDescription:UITextView!
    
    @IBOutlet weak var OrderLineView:UIView!
    
    var ComplainListArray:[String] = ["Finance" , "Technical" , "Address Issue" , "Other"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Register cell
        let nib = UINib(nibName: "ComplainTypeCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "ComplainTypeCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.txtOrderId.delegate = self
        self.txtDescription.delegate = self
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ContactViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ComplainListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width - 20
        return CGSize(width: screenWidth/2 , height: 45.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj = self.ComplainListArray[indexPath.row]
        
        let cell:ComplainTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier:"ComplainTypeCell" , for: indexPath) as! ComplainTypeCell
        
        if indexPath.row == 0 {
            cell.bgView.backgroundColor = .lightGray
            cell.lblComplainType.textColor = .white
        }
        
        cell.lblComplainType.text = obj
        cell.bgView.cornerRadius = cell.bgView.frame.height * 0.45
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let obj = self.MyCourseListArray![indexPath.row]
//        self.navigationDelegate?.didNavigate(course: obj)
    }
}
