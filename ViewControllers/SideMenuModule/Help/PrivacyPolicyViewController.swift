//
//  PrivacyPolicyViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 01/01/2021.
//

import UIKit

extension PrivacyPolicyViewController {
    
    func initialSetup() {
        
        self.title = "Privacy Policy"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

class PrivacyPolicyViewController: UIViewController {

    class func instantiateFromStoryboard() -> PrivacyPolicyViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PrivacyPolicyViewController
    }
    
    @IBOutlet weak var txtView:UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.loadData()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadData() {
        let text = "In order to complete your order successfully, JoeyCo™ requires your personal information. Under no circumstances will any of your personal data or personal identity be shared and will be used solely within JoeyCo. This information could possibly be used for statistical analysis and to create reports that will help us serve you better and could potentially be shared with affiliated parties, however your personal identity will not be compromised. In the unfortunate event of any legal complications, JoeyCo will be obliged to share your personal information with legal authorities. JoeyCo understands that your information shared with us is sensitive and thus acts in accordance with the Personal Information Protection and Electronic Documents Act Canada.\n\nAs the user, you have the option of either providing your postal code or clicking on the geolocator icon. All data available to use through the geolocator will be used internally for statistical analysis and to produce reports that will help us serve you better. In no way will any of your personal information be shared or your personal identity be compromised through this system.\n\nTo process your payment, your credit card information is required. Through our secure servers, your credit card information will remain confidential. Once your order has been confirmed and processed, there are no refunds. A five minute grace period is made available for you to make any additions or modifications to your order. This grace period, however , is at the discretion of the merchant if your order has already been processed by them. After your payment has been processed, it can or may take up to 15 days for the amount to be credited."
        self.txtView.text = text
    }


}
