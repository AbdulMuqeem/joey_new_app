//
//  SummaryDetailsViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 09/01/2021.
//

import UIKit

extension SummaryDetailsViewController {
    
    func initialSetup() {
        
        self.title = "Details"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
    }
    
}

class SummaryDetailsViewController: UIViewController {

    class func instantiateFromStoryboard() -> SummaryDetailsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SummaryDetailsViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
