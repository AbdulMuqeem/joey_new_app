//
//  SummaryViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit
import ActionSheetPicker_3_0

extension SummaryViewController {
    
    func initialSetup() {
        
        self.title = "Summary"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.headerView.cornerRadius = self.headerView.frame.height * 0.45
        self.summaryView.cornerRadius = self.summaryView.frame.height * 0.45
        self.ordersView.cornerRadius = self.ordersView.frame.height * 0.45
        self.shiftView.cornerRadius = self.shiftView.frame.height * 0.45
        self.routesView.cornerRadius = self.routesView.frame.height * 0.45
        
    }
    
    @IBAction func startDateAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Start Date", datePickerMode: .date , selectedDate: Date(), minimumDate: nil , maximumDate: Date() , doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy" //Show Date
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblStartDate.text = newDate
            
//            dateFormatter.dateFormat = "yyyy-MM-dd" // Send this format to server
//            let resultString = dateFormatter.string(from: date! as! Date)
//            var date = resultString + " 00:00:00"
//            date = date.localToUTC(incomingFormat: "yyyy-MM-dd HH:mm:ss", outGoingFormat: "yyyy-MM-dd HH:mm:ss")
//            self.startDate = date
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func endDateAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "End Date", datePickerMode: .date , selectedDate: Date(), minimumDate: nil , maximumDate: Date() , doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy" //Show Date
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblEndDate.text = newDate
            
//            dateFormatter.dateFormat = "yyyy-MM-dd" // Send this format to server
//            let resultString = dateFormatter.string(from: date! as! Date)
//            var date = resultString + " 23:59:59"
//            date = date.localToUTC(incomingFormat: "yyyy-MM-dd HH:mm:ss", outGoingFormat: "yyyy-MM-dd HH:mm:ss")
//            self.endDate = date
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
}

class SummaryViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SummaryViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SummaryViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var lblStartDate:UILabel!
    @IBOutlet weak var lblEndDate:UILabel!
    
    @IBOutlet weak var scrollView:UIView!
    @IBOutlet weak var tblView:UITableView!
    
    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var summaryView:UIView!
    @IBOutlet weak var ordersView:UIView!
    @IBOutlet weak var shiftView:UIView!
    @IBOutlet weak var routesView:UIView!
    
    @IBOutlet weak var lblSummary:UILabel!
    @IBOutlet weak var lblOrders:UILabel!
    @IBOutlet weak var lblShifts:UILabel!
    @IBOutlet weak var lblRoutes:UILabel!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var bottomView:UIView!
    
    var tableViewType:String = "Orders"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register cell
        let nib = UINib(nibName: "SummaryCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "SummaryCell")
        
        let nibRoutes = UINib(nibName: "RoutesCell", bundle: nil)
        self.tblView.register(nibRoutes, forCellReuseIdentifier: "RoutesCell")
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.bottomView {
            self.shadowView.isHidden = true
            self.bottomView.isHidden = true
        }
    }
    
    @IBAction func summaryAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.summaryView.backgroundColor = Theme_Orange_Color
        self.summaryView.BorderColor = Theme_Orange_Color
        self.summaryView.shadowColor = Theme_Orange_Color
        self.lblSummary.textColor = .white
        
        self.ordersView.backgroundColor = .white
        self.ordersView.BorderColor = Gray_Shadow_Color
        self.ordersView.shadowColor = Gray_Shadow_Color
        self.lblOrders.textColor = Theme_Green_Color
        
        self.shiftView.backgroundColor = .white
        self.shiftView.BorderColor = Gray_Shadow_Color
        self.shiftView.shadowColor = Gray_Shadow_Color
        self.lblShifts.textColor = Theme_Green_Color
        
        self.routesView.backgroundColor = .white
        self.routesView.BorderColor = Gray_Shadow_Color
        self.routesView.shadowColor = Gray_Shadow_Color
        self.lblRoutes.textColor = Theme_Green_Color
        
        self.scrollView.isHidden = false
        self.tblView.isHidden = true
        
    }
    
    @IBAction func ordersAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.ordersView.backgroundColor = Theme_Orange_Color
        self.ordersView.BorderColor = Theme_Orange_Color
        self.ordersView.shadowColor = Theme_Orange_Color
        self.lblOrders.textColor = .white
        
        self.summaryView.backgroundColor = .white
        self.summaryView.BorderColor = Gray_Shadow_Color
        self.summaryView.shadowColor = Gray_Shadow_Color
        self.lblSummary.textColor = Theme_Green_Color
        
        self.shiftView.backgroundColor = .white
        self.shiftView.BorderColor = Gray_Shadow_Color
        self.shiftView.shadowColor = Gray_Shadow_Color
        self.lblShifts.textColor = Theme_Green_Color
        
        self.routesView.backgroundColor = .white
        self.routesView.BorderColor = Gray_Shadow_Color
        self.routesView.shadowColor = Gray_Shadow_Color
        self.lblRoutes.textColor = Theme_Green_Color
        
        self.tableViewType = "Orders"
        self.scrollView.isHidden = true
        self.tblView.isHidden = false
        self.tblView.reloadData()
        
    }
    
    @IBAction func shiftsAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.shiftView.backgroundColor = Theme_Orange_Color
        self.shiftView.BorderColor = Theme_Orange_Color
        self.shiftView.shadowColor = Theme_Orange_Color
        self.lblShifts.textColor = .white
        
        self.summaryView.backgroundColor = .white
        self.summaryView.BorderColor = Gray_Shadow_Color
        self.summaryView.shadowColor = Gray_Shadow_Color
        self.lblSummary.textColor = Theme_Green_Color
        
        self.ordersView.backgroundColor = .white
        self.ordersView.BorderColor = Gray_Shadow_Color
        self.ordersView.shadowColor = Gray_Shadow_Color
        self.lblOrders.textColor = Theme_Green_Color
        
        self.routesView.backgroundColor = .white
        self.routesView.BorderColor = Gray_Shadow_Color
        self.routesView.shadowColor = Gray_Shadow_Color
        self.lblRoutes.textColor = Theme_Green_Color
        
        self.tableViewType = "Shifts"
        self.scrollView.isHidden = true
        self.tblView.isHidden = false
        self.tblView.reloadData()
        
    }
    
    @IBAction func routesAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.routesView.backgroundColor = Theme_Orange_Color
        self.routesView.BorderColor = Theme_Orange_Color
        self.routesView.shadowColor = Theme_Orange_Color
        self.lblRoutes.textColor = .white
        
        self.summaryView.backgroundColor = .white
        self.summaryView.BorderColor = Gray_Shadow_Color
        self.summaryView.shadowColor = Gray_Shadow_Color
        self.lblSummary.textColor = Theme_Green_Color
        
        self.ordersView.backgroundColor = .white
        self.ordersView.BorderColor = Gray_Shadow_Color
        self.ordersView.shadowColor = Gray_Shadow_Color
        self.lblOrders.textColor = Theme_Green_Color
        
        self.shiftView.backgroundColor = .white
        self.shiftView.BorderColor = Gray_Shadow_Color
        self.shiftView.shadowColor = Gray_Shadow_Color
        self.lblShifts.textColor = Theme_Green_Color
        
        self.tableViewType = "routes"
        self.scrollView.isHidden = true
        self.tblView.isHidden = false
        self.tblView.reloadData()
        
    }
}

extension SummaryViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.tableViewType == "routes" {
            return GISTUtility.convertToRatio(200.0)
        }
        return GISTUtility.convertToRatio(70.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.tableViewType == "routes" {
            let cell:RoutesCell =  tableView.dequeueReusableCell(withIdentifier: "RoutesCell", for: indexPath) as! RoutesCell
            return cell
        }
        else {
            let cell:SummaryCell =  tableView.dequeueReusableCell(withIdentifier: "SummaryCell", for: indexPath) as! SummaryCell
            
            if self.tableViewType == "Orders" {
                cell.lblOrderId.text = "Order#7689"
            }
            else {
                cell.lblOrderId.text = "Shift#7689"
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.tableViewType == "routes" {
        }
        else {
            self.shadowView.isHidden = false
            self.bottomView.isHidden = false
            
            // This line will animate all your constraint changes
            UIView.animate(withDuration: 0.4)
            {
                self.view.layoutIfNeeded()
            }
//            let vc = SummaryDetailsViewController.instantiateFromStoryboard()
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
