//
//  ItinaryOrdersViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit
import GoogleMaps

extension ItineraryOrdersViewController {
    
    func initialSetup() {
        
        self.title = "Ecommerce Orders"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
                
    }
    
}

extension ItineraryOrdersViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.googleMapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = googleMapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
        
        self.alertView.alertShow(title: "Access Permission", msg: "Joey App needs to access your location to show your current location", id: 1)
        self.alertView.isHidden = false
        
    }
    
}


class ItineraryOrdersViewController: UIViewController {

    class func instantiateFromStoryboard() -> ItineraryOrdersViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ItineraryOrdersViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var googleMapView:GMSMapView!
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var reportDelaytblView:UITableView!
    
    @IBOutlet weak var mapView:UIView!
    @IBOutlet weak var listView:UIView!
    
    @IBOutlet weak var lblMap:UILabel!
    @IBOutlet weak var lblList:UILabel!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var openMapDialogueView:UIView!
    @IBOutlet weak var infoDialogueView:UIView!
    @IBOutlet weak var delayDialogueView:UIView!
    
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    var isOpenMapDialogue:Bool! = false
    var isOpenInfoDialogue:Bool! = false
    var isOpenDelayDialogue:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Register cell
        let nib = UINib(nibName: "ItinaryOrderCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "ItinaryOrderCell")
        
        let nib1 = UINib(nibName: "ReportDelayCell", bundle: nil)
        self.reportDelaytblView.register(nib1, forCellReuseIdentifier: "ReportDelayCell")
        
        self.mapView.cornerRadius = self.mapView.frame.height * 0.45
        self.listView.cornerRadius = self.listView.frame.height * 0.45
        
        self.mapView.backgroundColor = Theme_Orange_Color
        self.mapView.BorderColor = Theme_Orange_Color
        self.mapView.shadowColor = Theme_Orange_Color
        self.lblMap.textColor = .white
        
        self.listView.backgroundColor = .white
        self.listView.BorderColor = Gray_Shadow_Color
        self.listView.shadowColor = Gray_Shadow_Color
        self.lblList.textColor = Theme_Green_Color
        
        self.shadowView.isHidden = true
        self.openMapDialogueView.isHidden = true
        self.infoDialogueView.isHidden = true
        self.delayDialogueView.isHidden = true
        
        self.googleMapView.isHidden = false
        self.tblView.isHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        self.getMap(33.7295, 73.0372)
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                
        let touch = touches.first
        
        if self.isOpenMapDialogue == true {
            if touch?.view != self.openMapDialogueView {
                self.shadowView.isHidden = true
                self.openMapDialogueView.isHidden = true
                self.isOpenMapDialogue = false
            }
        }
        else if self.isOpenInfoDialogue == true {
            if touch?.view != self.infoDialogueView {
                self.shadowView.isHidden = true
                self.infoDialogueView.isHidden = true
                self.isOpenInfoDialogue = false
            }
        }
        else if self.isOpenDelayDialogue == true {
            if touch?.view != self.delayDialogueView {
                self.shadowView.isHidden = true
                self.delayDialogueView.isHidden = true
                self.isOpenDelayDialogue = false
            }
        }

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.openMapDialogueView.clipsToBounds = true
        self.openMapDialogueView.layer.cornerRadius = 60
        self.openMapDialogueView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner] //Bottom
        
        self.infoDialogueView.clipsToBounds = true
        self.infoDialogueView.layer.cornerRadius = 60
        self.infoDialogueView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner] //top
        
        self.delayDialogueView.clipsToBounds = true
        self.delayDialogueView.layer.cornerRadius = 60
        self.delayDialogueView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner] //top

        
        self.view.layoutIfNeeded()
        
    }

    
    @IBAction func mapAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.mapView.backgroundColor = Theme_Orange_Color
        self.mapView.BorderColor = Theme_Orange_Color
        self.mapView.shadowColor = Theme_Orange_Color
        self.lblMap.textColor = .white
        
        self.listView.backgroundColor = .white
        self.listView.BorderColor = Gray_Shadow_Color
        self.listView.shadowColor = Gray_Shadow_Color
        self.lblList.textColor = Theme_Green_Color
        
        self.googleMapView.isHidden = false
        self.tblView.isHidden = false
        
    }
    
    @IBAction func listAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.listView.backgroundColor = Theme_Orange_Color
        self.listView.BorderColor = Theme_Orange_Color
        self.listView.shadowColor = Theme_Orange_Color
        self.lblList.textColor = .white
        
        self.mapView.backgroundColor = .white
        self.mapView.BorderColor = Gray_Shadow_Color
        self.mapView.shadowColor = Gray_Shadow_Color
        self.lblMap.textColor = Theme_Green_Color
        
        self.googleMapView.isHidden = true
        self.tblView.isHidden = false
        self.tblView.reloadData()
        
    }

}

//MARK:- TableView Delegate & DataSource
extension ItineraryOrdersViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblView {
            return 10
        }
        else {
            return 4
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tblView {
            if indexPath.row != 1 {
                return GISTUtility.convertToRatio(100.0)
            }
            return GISTUtility.convertToRatio(270.0)
        }
        else {
            return GISTUtility.convertToRatio(60.0)
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if tableView == self.tblView {
            let cell:ItinaryOrderCell = tableView.dequeueReusableCell(withIdentifier: "ItinaryOrderCell" , for: indexPath) as! ItinaryOrderCell
                    
            cell.delegate = self
            return cell
        }
        else {
            let cell:ReportDelayCell = tableView.dequeueReusableCell(withIdentifier: "ReportDelayCell" , for: indexPath) as! ReportDelayCell
                    
            if indexPath.row == 0 {
                cell.lblTitle.text = "Joey at pickup location"
            }
            else if indexPath.row == 1 {
                cell.lblTitle.text = "Delay at pickup location"
            }
            else if indexPath.row == 2 {
                cell.lblTitle.text = "Joey incident"
            }
            else if indexPath.row == 3 {
                cell.lblTitle.text = "Delivery to hub for re-delivery"
            }
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != self.tblView {
            self.shadowView.isHidden = true
            self.delayDialogueView.isHidden = true
        }
    }
}

//MARK:- Delegate Actions
extension ItineraryOrdersViewController : ItinaryOrderDelegate {
    
    func mapAction() {
        
        self.shadowView.isHidden = false
        self.openMapDialogueView.isHidden = false
        self.isOpenMapDialogue = true
        
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
    }
    
    func callAction() {
        let vc = CallViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func infoAction() {
        
        self.shadowView.isHidden = false
        self.infoDialogueView.isHidden = false
        self.isOpenInfoDialogue = true
        
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }

    }
    
    func pickupScanAction() {
        self.showBanner(title: "Alert", subTitle: "Scanner screen will be open in beta build", type: WARNING)
    }
    
    func dropoffScanAction() {
        self.showBanner(title: "Alert", subTitle: "Scanner screen will be open in beta build", type: WARNING)
    }
    
    func delayAction() {
        
        self.shadowView.isHidden = false
        self.delayDialogueView.isHidden = false
        self.isOpenDelayDialogue = true
        
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }

    }
    
    @IBAction func cancelReportDelayAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.shadowView.isHidden = true
        self.delayDialogueView.isHidden = true
        self.isOpenDelayDialogue = false
    }
    
}
