//
//  CallViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 14/06/2021.
//

import UIKit

class CallViewController: UIViewController {

    class func instantiateFromStoryboard() -> CallViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CallViewController
    }
    
    @IBOutlet weak var lblTimer:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.HideNavigationBar()
        
    }

    @IBAction func endCallAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
