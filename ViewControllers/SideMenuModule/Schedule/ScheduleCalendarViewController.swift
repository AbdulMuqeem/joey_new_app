//
//  ScheduleCalendarViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 19/01/2021.
//

import UIKit
import FSCalendar

extension ScheduleCalendarViewController {
    
    func initialSetup() {
        
        self.title = self.text
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
    }
    
}

class ScheduleCalendarViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ScheduleCalendarViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ScheduleCalendarViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var emptyView:UIView!
    
    @IBOutlet weak var calendarView:FSCalendar!
    var text:String! = "Schedule"
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var bottomViewHeight:NSLayoutConstraint!

    @IBOutlet weak var lblBottomTitle:UILabel!
    @IBOutlet weak var lblBottomMessage:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.calendarView.scope = .week

        self.emptyView.isHidden = false
        self.tblView.isHidden = true
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
                
        if self.text == "Full Schedule" {
            self.lblBottomTitle.text = "Can not remove"
            self.lblBottomMessage.text = "This schedule can not be removed, because either the shift is active or about to start"
        }
        else {
            self.lblBottomTitle.text = "Confirmation"
            self.lblBottomMessage.text = "Are you sure you want to schedule this shift ?"
        }
        
        //Register cell
        let nib = UINib(nibName: "ScheduleCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "ScheduleCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.roundCorners(corners: [.topRight , .topLeft], radius: 60)
        self.view.layoutIfNeeded()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.bottomView {
            self.shadowView.isHidden = true
            self.bottomView.isHidden = true
        }
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- Calendar Delegate & DataSource
extension ScheduleCalendarViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        let newDate = date.addingTimeInterval(TimeInterval(NSTimeZone.local.secondsFromGMT()))
        print(newDate)
        
        let FSdate = newDate
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateFormat = "yyyy-MM-dd"
        let newFSdate = format.string(from: FSdate)
        print(newFSdate)
        
        return true
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.emptyView.isHidden = true
        self.tblView.isHidden = false
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("Tapped")
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("Tapped")
    }
}

//MARK:- TableView Delegate & DataSource
extension ScheduleCalendarViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(120.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ScheduleCell =  tableView.dequeueReusableCell(withIdentifier: "ScheduleCell", for: indexPath) as! ScheduleCell
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.acceptAction()
    }
    
}

//MARK:- BottomView Functionality
extension ScheduleCalendarViewController {
    
    func acceptAction() {
        
//        self.bottomViewHeight.constant = 300.0
        self.shadowView.isHidden = false
        self.bottomView.isHidden = false
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func cancelAction(_ sender : UIButton) {
        
//        self.bottomViewHeight.constant = 0.0
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func okAction(_ sender : UIButton) {
        
//        self.bottomViewHeight.constant = 0.0
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        // This line will animate all your constraint changes
        UIView.animate(withDuration: 0.4)
        {
            self.view.layoutIfNeeded()
        }
        
//        self.showBanner(title: "Success!", subTitle: "Order has been accepted", type: SUCCESS)
    }
    
}
