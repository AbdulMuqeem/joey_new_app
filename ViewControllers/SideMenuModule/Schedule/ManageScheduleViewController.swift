//
//  ManageScheduleViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 19/01/2021.
//

import UIKit
import GoogleMaps

extension ManageScheduleViewController {
    
    func initialSetup() {
        
        self.title = "Manage Schedule"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.mapView.delegate = self
        //        self.didAccesToLocation()
        //        self.determineMyCurrentLocation()

        self.getMap(33.7295, 73.0372)
        
    }
    
}

extension ManageScheduleViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
//        self.ShowAlert(title: "Access Permission", message: "Goldfin App needs to access your location to show your current location") { (sender) in
//
//            let url = URL(string: UIApplication.openSettingsURLString)!
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//
//        }
        
    }
    
}

class ManageScheduleViewController: UIViewController {

    class func instantiateFromStoryboard() -> ManageScheduleViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ManageScheduleViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var mapView:GMSMapView!
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var bottomViewHeight:NSLayoutConstraint!
    
    @IBOutlet weak var selectZoneView:UIView!
    @IBOutlet weak var selectVehicleView:UIView!
    
    @IBOutlet weak var collectionViewZone:UICollectionView!
    @IBOutlet weak var collectionViewVehicle:UICollectionView!
    
    var ZoneListArray:[String] = ["Canada" , "Otawa" , "Toronto" , "Calgary"]
    var VehicleNameListArray:[String] = ["Bicycle" , "Scooter" , "Car" , "Truck" , "SUV" , "Van"]
    var VehicleImageListArray:[UIImage] = [UIImage(named:"cycle")! , UIImage(named:"bike")! , UIImage(named:"car_home")! , UIImage(named:"truck_home")! , UIImage(named:"suv_home")! , UIImage(named:"van_home")!]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register cell
        let nib = UINib(nibName: "HomeVehicleTypeCell", bundle: nil)
        self.collectionViewVehicle.register(nib, forCellWithReuseIdentifier: "HomeVehicleTypeCell")
        
        let xib = UINib(nibName: "ZoneCell", bundle: nil)
        self.collectionViewZone.register(xib, forCellWithReuseIdentifier: "ZoneCell")
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        self.bottomView.roundCorners(corners: [.topRight , .topLeft], radius: 60)
        self.selectZoneView.roundCorners(corners: [.topRight , .bottomRight], radius: 20)
        self.selectVehicleView.roundCorners(corners: [.topRight , .bottomRight], radius: 20)
        self.view.layoutIfNeeded()

    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func slotsAction(_ sender : UIButton) {
        let vc = ScheduleCalendarViewController.instantiateFromStoryboard()
        vc.text = "Available Slots"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ManageScheduleViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewZone {
            return self.ZoneListArray.count
        }
        return self.VehicleNameListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionViewZone {
            
            let text = self.ZoneListArray[indexPath.row]
                        
            let font = UIFont(name: "Poppins-Medium" , size: 13.0)!
            let width = self.estimatedFrame(text: text, font: font).width + 40
            return CGSize(width: width, height: 45.0)

        }
        
        let screenWidth =  collectionView.bounds.size.width //- 20
        let height = GISTUtility.convertToRatio(130.0)
        let width = GISTUtility.convertToRatio(screenWidth/2.7)
        return CGSize(width: width , height: height)

        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionViewZone {
            
            let obj = self.ZoneListArray[indexPath.row]
            
            let cell:ZoneCell = collectionView.dequeueReusableCell(withReuseIdentifier:"ZoneCell" , for: indexPath) as! ZoneCell
            
            cell.bgView.backgroundColor = UIColor.init(rgb: 0xF1F9F1)
            cell.bgView.shadowColor = UIColor.init(rgb: 0xF1F9F1)
            cell.bgView.BorderColor = UIColor.init(rgb: 0xF1F9F1)
            cell.lblComplainType.textColor = Theme_Green_Color
            
            cell.lblComplainType.text = obj
            
            if indexPath.row == 0 {
                cell.bgView.backgroundColor = Theme_Green_Color
                cell.lblComplainType.textColor = .white
            }

            cell.bgView.cornerRadius = 45.0 * 0.45
            return cell
            
        }
        
        let cell:HomeVehicleTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier:"HomeVehicleTypeCell" , for: indexPath) as! HomeVehicleTypeCell
        
        cell.bgView.clipsToBounds = true
        cell.bgView.layer.cornerRadius = GISTUtility.convertToRatio(60.0)
        cell.bgView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        cell.bgView.layoutIfNeeded()

        cell.lblVehicleName.text = self.VehicleNameListArray[indexPath.row]
        cell.imgVehicle.image = self.VehicleImageListArray[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        let obj = self.MyCourseListArray![indexPath.row]
        //        self.navigationDelegate?.didNavigate(course: obj)
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let height = 45.0
        let size = CGSize(width: 0.0, height: height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
}
