//
//  ScheduleViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit
import GoogleMaps

extension ScheduleViewController {
    
    func initialSetup() {
        
        self.title = "Schedule Shift"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.mapView.delegate = self
        //        self.didAccesToLocation()
        //        self.determineMyCurrentLocation()

        self.getMap(33.7295, 73.0372)
        
    }
    
}

extension ScheduleViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
//        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
//        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
//
//        self.alertView.alertShow(title: "Access Permission", msg: "Joey App require access to your location, Enable location services for more accurate location", id: 1)
//        self.alertView.isHidden = false
        
    }
    
}

class ScheduleViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ScheduleViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ScheduleViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var mapView:GMSMapView!
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var bottomTimeView:UIView!
    @IBOutlet weak var bottomViewHeight:NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//
//        //Swipe Gesture
//        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeUp.direction = .up
//        self.bottomView.addGestureRecognizer(swipeUp)
//
//        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeDown.direction = .down
//        self.bottomView.addGestureRecognizer(swipeDown)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
        self.bottomViewHeight.constant  = 250.0
        self.bottomView.isHidden = false

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.roundCorners(corners: [.topRight , .topLeft], radius: 60)
        self.bottomTimeView.roundCorners(corners: [.topRight , .topLeft , .bottomRight], radius: 40)
        self.view.layoutIfNeeded()
        
    }
    
//    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
//
//        if gesture.direction == .up {
//            self.bottomViewHeight.constant  = 300.0
//            self.bottomView.backgroundColor = .white
//            self.bottomView.isHidden = false
//        }
//        else if gesture.direction == .down {
//            self.bottomViewHeight.constant  = 100.0
//        }
//
//        // This line will animate all your constraint changes
//        UIView.animate(withDuration: 0.3)
//        {
//            self.view.layoutIfNeeded()
//        }
//    }
    
    @IBAction func fullScheduleAction(_ sender : UIButton) {
        let vc = ScheduleCalendarViewController.instantiateFromStoryboard()
        vc.text = "Full Schedule"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func manageScheduleAction(_ sender : UIButton) {
        let vc = ManageScheduleViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
