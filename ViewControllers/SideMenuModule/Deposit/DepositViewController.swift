//
//  DepositViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 30/12/2020.
//

import UIKit
import ActionSheetPicker_3_0

extension DepositViewController {
    
    func initialSetup() {
        
        self.title = "Deposit"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
              
        self.txtAmount.delegate = self
        self.txtCashHand.delegate = self
        self.txtDepositPending.delegate = self
        self.txtDepositAmount.delegate = self
        self.txtDate.delegate = self
        
        self.txtAmount.attributedPlaceholder = NSAttributedString(string: "Enter Amount", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtCashHand.attributedPlaceholder = NSAttributedString(string: "Cash on hand", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtDepositPending.attributedPlaceholder = NSAttributedString(string: "Deposit pending approval", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtDepositAmount.attributedPlaceholder = NSAttributedString(string: "Amount to deposit", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtDate.attributedPlaceholder = NSAttributedString(string: "Select Date", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtAmount.text = "150.0 $"
        self.txtCashHand.text = "10.0 $"
        self.txtDepositPending.text = "10.0 $"
        self.txtDepositAmount.text = "10.0 $"
        self.txtDate.text = self.getCurrentDate()
        
    }
    
}

extension DepositViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.amountLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 2 {
            self.cashOnHandLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 3 {
            self.pendingLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 4 {
            self.depositLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        
        self.datetLineView.backgroundColor = UIColor.darkGray
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.amountLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 2 {
            self.cashOnHandLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 3 {
            self.pendingLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 4 {
            self.depositLineView.backgroundColor = UIColor.darkGray
        }
        
        return true
    }
    
}


class DepositViewController: UIViewController {

    class func instantiateFromStoryboard() -> DepositViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DepositViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var txtAmount:UITextField!
    @IBOutlet weak var txtCashHand:UITextField!
    @IBOutlet weak var txtDepositPending:UITextField!
    @IBOutlet weak var txtDepositAmount:UITextField!
    @IBOutlet weak var txtDate:UITextField!
    
    @IBOutlet weak var amountLineView:UIView!
    @IBOutlet weak var cashOnHandLineView:UIView!
    @IBOutlet weak var pendingLineView:UIView!
    @IBOutlet weak var depositLineView:UIView!
    @IBOutlet weak var datetLineView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initialSetup()
        self.LightStatusBar()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func dateAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        self.datetLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        
        ActionSheetDatePicker.show(withTitle: "Select Date", datePickerMode: .date , selectedDate: Date(), minimumDate: nil , maximumDate: Date() , doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtDate.text = newDate
            self.datetLineView.backgroundColor = UIColor.darkGray
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
}
