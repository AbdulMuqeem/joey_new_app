//
//  NotificationViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 30/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension NotificationViewController: AlertViewDelegate {
    
    func initialSetup() {
        
        self.title = "Notifications"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        self.lblNoData.isHidden = true
        self.tblView.isHidden = true
        
    }
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class NotificationViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> NotificationViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NotificationViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var lblNoData:UILabel!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    var isOpenSideMenu:Bool = false
    
//    var NotificationListArray:[NotificationList]? = [NotificationList]()
    var page = 1
    var perPage = 20

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.getNotificationList()
        
        //Register Cell
        let cell = UINib(nibName: "NotificationCell", bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: "NotificationCell")
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    //MARK:- Get Notification List Api
    func getNotificationList() {
        
//        self.view.endEditing(true)
//        self.startLoading(message: "")
//
//        if !AppHelper.isConnectedToInternet() {
//            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
//            self.stopLoading()
//            return
//        }
//
//        let params:[String:Any] = ["page": self.page , "limit": self.perPage]
//        print("Parameter: \(params)")
//
//        Services.GetNotifications(param: params , completionHandler: {(status, response, error) in
//
//            if !status {
//                if error != nil {
//                    let error = String(describing: (error as AnyObject).localizedDescription)
//                    print("Error: \(error)")
//                    self.alertView.alertShow(title: "Error", msg: ERROR_MESSAGE , id: 0)
//                    self.lblNoData.isHidden = false
//                    self.tblView.isHidden = true
//                    self.alertView.isHidden = false
//                    self.stopLoading()
//                    return
//                }
//                let msg = response?["message"].stringValue
//                print("Message: \(String(describing: msg!))")
//                self.alertView.alertShow(title: "Alert", msg: msg! , id: 0)
//                self.lblNoData.isHidden = false
//                self.tblView.isHidden = true
//                self.alertView.isHidden = false
//                self.stopLoading()
//                return
//            }
//
//            self.stopLoading()
//            print(response!)
//
//            if let data = response!["body"].array {
//                for a in data {
//                    let obj = NotificationList(json: a)
//                    self.NotificationListArray?.append(obj)
//                }
//            }
//
//            if self.NotificationListArray!.count == 0 {
//                self.lblNoData.isHidden = false
//                self.tblView.isHidden = true
//                return
//            }
//
//            self.lblNoData.isHidden = true
//            self.tblView.isHidden = false
//            self.tblView.reloadData()
//
//        })
        
        self.tblView.isHidden = false
        self.tblView.reloadData()
        
    }
    
}


extension NotificationViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4 //self.NotificationListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

//        if (indexPath.row == (self.NotificationListArray!.count) - 1) && ((self.NotificationListArray!.count) % 20) == 0 {
//            print("Load more")
//
//            self.page += 1
//            self.getNotificationList()
//
//        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let obj = self.NotificationListArray![indexPath.row]
        
        let cell:NotificationCell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell" , for: indexPath) as! NotificationCell
        
        cell.lblTitle.text = "Notification Title"
        cell.lblDescription.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting"
        cell.lblDate.text = "Jun 25 2021"
        cell.lblTime.text = "11:20 PM"
        
//        if obj.notificationData != nil {
//            if let title = obj.notificationData!.title {
//                cell.lblTitle.text = title
//            }
//
//            if let message = obj.notificationData!.message {
//                cell.lblDescription.text = message
//            }
//        }
//
//        if let value = obj.createdAt {
//
//            let date = value.UTCToLocal(incomingFormat: "yyyy-MM-dd HH:mm:ss", outGoingFormat: "MMM dd yyyy")
//            let time = value.UTCToLocal(incomingFormat: "yyyy-MM-dd HH:mm:ss", outGoingFormat: "hh:mm a")
//
//            if date != "" {
//                cell.lblDate.text = date
//            }
//
//            if time != "" {
//                cell.lblTime.text = time
//            }
//
//        }
        
        return cell
        
    }
    
}


