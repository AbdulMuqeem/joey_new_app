//
//  SideMenuViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    
    var itemsLogin: [String] = ["Home" , "Profile" , "Notifications" , "Grocerry Orders" , "Schedule" , "Summary", "Ecommerce Orders" , "Document" , "Help" , "Logout"]
    var imagesLogin: [UIImage] = [UIImage(named:"home")! , UIImage(named:"profile")! , UIImage(named:"notification")! , UIImage(named:"grocery-orders")! , UIImage(named:"schedules")! , UIImage(named:"summary")! , UIImage(named:"ecommerce-orders")! , UIImage(named:"summary")! , UIImage(named:"help")! , UIImage(named:"logout")! ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Register cell
        let nib = UINib(nibName: "SideMenuTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SideMenuTableViewCell")
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileUpdate), name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
        
//        self.getEmailUpdate()
//        self.getProfileUpdate()

    }
    
//
//    @objc func getProfileUpdate() {
//
//        if let image = Singleton.sharedInstance.CurrentUser!.profileImage {
//            self.imgLogo.setImageFromUrl(urlStr: image)
//        }
//
//        if let firstName = Singleton.sharedInstance.CurrentUser!.firstName {
//            let lastName = Singleton.sharedInstance.CurrentUser!.lastName!
//            self.lblName.text = firstName + " " + lastName
//        }
//
//        if let education = Singleton.sharedInstance.CurrentUser!.educationType {
//            self.lblDegree.text = education
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.LightStatusBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.profileView.roundCorners(corners: [.bottomRight], radius: 50)
        self.view.layoutIfNeeded()
        
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsLogin.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(50)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        
        cell.lblTitle.text = self.itemsLogin[indexPath.row]
        cell.imgView.image = self.imagesLogin[indexPath.row]
        cell.lineView.isHidden = true
        
        if indexPath.row == 7 {
            cell.lineView.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 1 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = MyProfileViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 2 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = NotificationViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 3 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = OrdersViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 4 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = ScheduleViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 5 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = SummaryViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 6 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = ItineraryOrdersViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 7 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = DocumentListViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 8 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HelpViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 9 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            vc.isLogout = true
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        }
        
    }
    
}





