//
//  TutorialTwoViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 16/12/2020.
//

import UIKit

class TutorialTwoViewController: UIViewController {

    class func instantiateFromStoryboard() -> TutorialTwoViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TutorialTwoViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.initialSetup()
        
        //Swipe Gesture
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
    }

    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .right {
            self.navigationController?.popViewController(animated: true)
        }
        else if gesture.direction == .left {
            let vc = LoginViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender : UIButton) {
        let vc = LoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func skipAction(_ sender : UIButton) {
        let vc = LoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
        
    
}
