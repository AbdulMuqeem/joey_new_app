//
//  SelectTutorialTypeViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 11/05/2021.
//

import UIKit

class SelectTutorialTypeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SelectTutorialTypeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SelectTutorialTypeViewController
    }

    @IBOutlet weak var stepView: UIImageView!
    @IBOutlet weak var topBackkgroundViewConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var vendorView: UIView!
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var watchTutorialsView: UIView!
    @IBOutlet weak var startReadingView: UIView!
    
    @IBOutlet weak var btnStartTour: UIButton!
    var isHelp:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    func initialSetup() {
        
        if self.isHelp == true {
            
            self.title = "Training"
            self.navigationController?.ShowNavigationBar()
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
            self.navigationController?.ThemedNavigationBar()
            self.navigationController?.ChangeTitleFont()
            
            self.stepView.isHidden = true
            self.topBackkgroundViewConstraints.constant = 0.0
            
        }
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.btnStartTour.setTitle("Start\nTour", for: .normal)
        self.btnStartTour.titleLabel?.lineBreakMode = .byWordWrapping
        self.btnStartTour.titleLabel?.textAlignment = .center
        
        self.categoryView.layer.cornerRadius = 10
        self.categoryView.clipsToBounds = true
        
        self.vendorView.layer.cornerRadius = 10
        self.vendorView.clipsToBounds = true
        
        //BottomView Outlets
        self.watchTutorialsView.layer.cornerRadius = 25
        self.watchTutorialsView.clipsToBounds = true
        
        self.startReadingView.layer.cornerRadius = 25
        self.startReadingView.clipsToBounds = true
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
    }
    
    @IBAction func btnActionCategory(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
    }
    
    @IBAction func btnActionVendor(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
    }
    
    @IBAction func btnActionStartTour(_ sender: Any) {
        let vc = TutorialTourView.instantiateFromStoryboard()
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //BottomView Actions
    @IBAction func btnActionWatchTutorial(_ sender: Any) {
        let vc = WatchTutorialsViewController.instantiateFromStoryboard()
        if self.isHelp == true {
            vc.isHelp = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActionStartReading(_ sender: Any) {
        let vc = StartReadigViewController.instantiateFromStoryboard()
        if self.isHelp == true {
            vc.isHelp = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
