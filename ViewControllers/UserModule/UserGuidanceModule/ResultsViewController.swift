//
//  ResultsViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 18/05/2021.
//

import UIKit

class ResultsViewController: UIViewController {

    class func instantiateFromStoryboard() -> ResultsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ResultsViewController
    }
    /*
     ***********
     ***********
     will set labels and top image according to failed and success scenarios
     ***********
     ***********
     */
    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var topImage: UIImageView!
   
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var lblResultDesc: UILabel!
    
    @IBOutlet weak var questionDetailsView: UIView!
    @IBOutlet weak var outerProgressView: UIView!
    @IBOutlet weak var innerSuccessView: UIView!
    @IBOutlet weak var innerFailedView: UIView!

    @IBOutlet weak var lblSuccessPercentage: UILabel!
    @IBOutlet weak var lblFailedPercentage: UILabel!
    
    @IBOutlet weak var lblRightAnswers: UILabel!
    @IBOutlet weak var lblTotalQuestions: UILabel!
    
    @IBOutlet weak var checkAnswerView: UIView!
    @IBOutlet weak var btnCheckAnswers: UIButton!
    
    @IBOutlet weak var lblBottomResultDesc: UILabel!
    
    @IBOutlet weak var btnBottom: UIButton!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)
        
        //Register cell (bottomView)
        
        self.bottomTableView.delegate = self
        self.bottomTableView.dataSource = self
        
        let nib = UINib(nibName: "AnswersCell", bundle: nil)
        self.bottomTableView.register(nib, forCellReuseIdentifier: "AnswersCell")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
    }
    
    func initialSetup() {
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.questionDetailsView.layer.cornerRadius = 10
        self.questionDetailsView.clipsToBounds = true
        
        self.outerProgressView.layer.cornerRadius = 10
        self.outerProgressView.clipsToBounds = true
        
        self.innerSuccessView.layer.cornerRadius = 10
        self.innerSuccessView.clipsToBounds = true
        
        self.innerFailedView.layer.cornerRadius = 10
        self.innerFailedView.clipsToBounds = true
        
        self.checkAnswerView.layer.cornerRadius = 20
        self.checkAnswerView.clipsToBounds = true
    }
    
    @IBAction func btnActionCheckAnswers(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
    }
    
    @IBAction func btnActionbottom(_ sender: Any) {
        let controller = SideMenuRootViewController.instantiateFromStoryboard()
        controller.leftViewPresentationStyle = .slideAbove
        if #available(iOS 13.0, *) {
            SceneDelegate.getInstatnce().window?.rootViewController = controller
        }
        else {
            AppDelegate.getInstatnce().window?.rootViewController = controller
        }
    }
    
    @IBAction func btnActionGoBackToHome(_ sender: Any) {
        
    }
    
}

extension ResultsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(84.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell:AnswersCell =  tableView.dequeueReusableCell(withIdentifier: "AnswersCell", for: indexPath) as! AnswersCell

//        if indexPath.row == 0 {
//            cell.lblDocumentType.text = "Drivers's License"
//            cell.lblExpiryData.text = "Expiry: 23-02-2022"
//        }
//        else if indexPath.row == 1 {
//            cell.lblDocumentType.text = "Additional Document 3"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 2 {
//            cell.lblDocumentType.text = "Test"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 3 {
//            cell.lblDocumentType.text = "Vehicle Insurance"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 4 {
//            cell.lblDocumentType.text = "Additional Document 1"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 5 {
//            cell.lblDocumentType.text = "Additional Document 2"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        cell.indexpath = indexPath
//        cell.mainView.dropShadow()
        
        return cell
        
    }
    
}
