//
//  DocumentDetailsViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 11/05/2021.
//

import UIKit

class DocumentDetailsViewController: UIViewController {

    class func instantiateFromStoryboard() -> DocumentDetailsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DocumentDetailsViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnStartTour: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnStartTour.setTitle("START\nTOUR", for: .normal)
        self.btnStartTour.titleLabel?.lineBreakMode = .byWordWrapping
        self.btnStartTour.titleLabel?.textAlignment = .center


        //Register cell
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let nib = UINib(nibName: "DocumentCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "DocumentCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.HideNavigationBar()
    }
    
    @IBAction func btnActionProceed(_ sender: Any) {
        let vc = SelectTutorialTypeViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnActionStartTour(_ sender: Any) {
        let vc = DocumentTakeTourView.instantiateFromStoryboard()
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}


extension DocumentDetailsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell:DocumentCell =  tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath) as! DocumentCell

        if indexPath.row == 0 {
            cell.lblDocumentType.text = "SIN"
            cell.lblExpiryData.text = "Expiry: 23 October, 2023"
        }
        else if indexPath.row == 1 {
            cell.lblDocumentType.text = "Driver's Permit"
            cell.lblExpiryData.text = "Expiry: 23 October, 2023"
        }
        else if indexPath.row == 2 {
            cell.lblDocumentType.text = "Driver's License"
            cell.lblExpiryData.text = "Expiry: 02-03-2023"
        }
        else if indexPath.row == 3 {
            cell.lblDocumentType.text = "Vehicle Insurance"
            cell.lblExpiryData.text = "Expiry: 02-03-2023"
        }
        else if indexPath.row == 4 {
            cell.lblDocumentType.text = "Additional Document 1"
            cell.lblExpiryData.text = "Expiry: 02-03-2023"
        }
        else if indexPath.row == 5 {
            cell.lblDocumentType.text = "Additional Document 2"
            cell.lblExpiryData.text = "Expiry: 02-03-2023"
        }
        
        cell.delegate = self
        cell.indexpath = indexPath
        cell.mainView.dropShadow()
        
        return cell
        
    }
    
}

extension DocumentDetailsViewController : UploadDocumentDelegate {
    
    func uploadDocumetAction(indexpath: IndexPath) {
        
        let vc = UploadDocumentViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
