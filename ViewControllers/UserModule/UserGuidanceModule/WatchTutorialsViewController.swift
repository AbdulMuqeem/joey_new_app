//
//  WatchTutorialsViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 16/05/2021.
//

import UIKit
import WebKit

class WatchTutorialsViewController: UIViewController {

    class func instantiateFromStoryboard() -> WatchTutorialsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WatchTutorialsViewController
    }
    
    @IBOutlet weak var btnReadyForTest: UIButton!
    @IBOutlet weak var stepView: UIImageView!
    
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    
    @IBOutlet weak var videoView: UIView!
    var webPlayer: WKWebView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    //BottomView outlets
    @IBOutlet weak var groceryView: UIView!
    @IBOutlet weak var medicalView: UIView!
    @IBOutlet weak var foodView: UIView!
    @IBOutlet weak var logisticView: UIView!
    
    @IBOutlet weak var groceryLbl: UILabel!
    @IBOutlet weak var medicalLbl: UILabel!
    @IBOutlet weak var foodLbl: UILabel!
    @IBOutlet weak var logisticLbl: UILabel!
    var isHelp:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //Register cell
        let nib = UINib(nibName: "WatchTutorialCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "WatchTutorialCell")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)
        
        let url = "https://www.youtube.com/embed/WJfCJsZvrVc?playsinline=1"
        self.setupWebViewPlayer(url:url)
        
    }
    
    func initialSetup() {
        
        if self.isHelp == true {
            
            self.title = "Training Videos"
            self.navigationController?.ShowNavigationBar()
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
            self.navigationController?.ThemedNavigationBar()
            self.navigationController?.ChangeTitleFont()
            
            self.btnReadyForTest.isHidden = true
            self.stepView.isHidden = true            
        }

        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.categoryView.layer.cornerRadius = 25
        self.categoryView.clipsToBounds = true
        
        self.groceryView.layer.cornerRadius = 30
        self.groceryView.clipsToBounds = true

        self.medicalView.layer.cornerRadius = 30
        self.medicalView.clipsToBounds = true
        
        self.foodView.layer.cornerRadius = 30
        self.foodView.clipsToBounds = true
        
        self.logisticView.layer.cornerRadius = 30
        self.logisticView.clipsToBounds = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionCategory(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
    }
    
    @IBAction func btnActionIAmReadyForTest(_ sender: Any) {
        let vc = QuizViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //BottomView Actions
    @IBAction func btnActionGrocery(_ sender: Any) {
        self.setUIViewOfCategory(selectedOption: 1)
    }
    
    @IBAction func btnActionMedical(_ sender: Any) {
        self.setUIViewOfCategory(selectedOption: 2)
    }

    @IBAction func btnActionFood(_ sender: Any) {
        self.setUIViewOfCategory(selectedOption: 3)
    }
    
    @IBAction func btnActionLogistic(_ sender: Any) {
        self.setUIViewOfCategory(selectedOption: 4)
        
    }
    
    //Helper Function
    func setUIViewOfCategory(selectedOption: Int) {
        if (selectedOption == 1) {
            self.groceryView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.groceryLbl.textColor = UIColor.white
            self.medicalLbl.textColor = UIColor.black
            self.foodLbl.textColor = UIColor.black
            self.logisticLbl.textColor = UIColor.black
            
            self.medicalView.backgroundColor = UIColor .white
            self.foodView.backgroundColor =  UIColor .white
            self.logisticView.backgroundColor =  UIColor .white
        }
        
        else if (selectedOption == 2) {
            self.groceryView.backgroundColor =  UIColor .white
            self.medicalView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.foodView.backgroundColor =  UIColor .white
            self.logisticView.backgroundColor =  UIColor .white
            self.groceryLbl.textColor = UIColor.black
            self.medicalLbl.textColor = UIColor.white
            self.foodLbl.textColor = UIColor.black
            self.logisticLbl.textColor = UIColor.black
        }
        
        else if (selectedOption == 3) {
            self.groceryView.backgroundColor =  UIColor .white
            self.medicalView.backgroundColor = UIColor .white
            self.foodView.backgroundColor =  UIColor.init(rgb: THEME_COLOR)
            self.logisticView.backgroundColor =  UIColor .white
            self.groceryLbl.textColor = UIColor.black
            self.medicalLbl.textColor = UIColor.black
            self.foodLbl.textColor = UIColor.white
            self.logisticLbl.textColor = UIColor.black
        }
        
        else {
            self.groceryView.backgroundColor =  UIColor .white
            self.medicalView.backgroundColor = UIColor .white
            self.foodView.backgroundColor =  UIColor .white
            self.logisticView.backgroundColor =  UIColor.init(rgb: THEME_COLOR)
            self.groceryLbl.textColor = UIColor.black
            self.medicalLbl.textColor = UIColor.black
            self.foodLbl.textColor = UIColor.black
            self.logisticLbl.textColor = UIColor.white
        }
    }
}

extension WatchTutorialsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(85.0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell:WatchTutorialCell =  tableView.dequeueReusableCell(withIdentifier: "WatchTutorialCell", for: indexPath) as! WatchTutorialCell

//        if indexPath.row == 0 {
//            cell.lb.text = "Drivers's License"
//            cell.lblExpiryData.text = "Expiry: 23-02-2022"
//        }
//        else if indexPath.row == 1 {
//            cell.lblDocumentType.text = "Additional Document 3"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 2 {
//            cell.lblDocumentType.text = "Test"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 3 {
//            cell.lblDocumentType.text = "Vehicle Insurance"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 4 {
//            cell.lblDocumentType.text = "Additional Document 1"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        else if indexPath.row == 5 {
//            cell.lblDocumentType.text = "Additional Document 2"
//            cell.lblExpiryData.text = "Expiry: 02-03-2023"
//        }
//        cell.indexpath = indexPath
//        cell.mainView.dropShadow()
        
        return cell
        
    }
    
}

//MARK:- Video Player
extension WatchTutorialsViewController {
    
    func setupWebViewPlayer(url:String) {
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.allowsInlineMediaPlayback = true

        DispatchQueue.main.async {
            
            self.webPlayer = WKWebView(frame: self.videoView.bounds, configuration: webConfiguration)
            self.webPlayer.contentMode = .scaleAspectFit
            self.videoView.addSubview(self.webPlayer)
            
            guard let videoURL = URL(string: url) else {
                self.videoView.isHidden = true
                return
            }
            let request = URLRequest(url: videoURL)
            self.webPlayer.load(request)
            
        }
    }
}
