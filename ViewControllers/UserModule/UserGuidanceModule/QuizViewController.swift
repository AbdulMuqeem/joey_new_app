//
//  QuizViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 17/05/2021.
//

import UIKit

class QuizViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> QuizViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! QuizViewController
    }
    
    @IBOutlet weak var outerProgressView: UIView!
    @IBOutlet weak var innerProgressView: UIView!
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    @IBOutlet weak var firstOptionView: UIView!
    @IBOutlet weak var lblFirstOption: UILabel!
    
    @IBOutlet weak var secondOptionView: UIView!
    @IBOutlet weak var lblSecondOption: UILabel!
    
    @IBOutlet weak var thirdOptionView: UIView!
    @IBOutlet weak var lblThirdOption: UILabel!
    
    @IBOutlet weak var forthOptionView: UIView!
    @IBOutlet weak var lblForthOption: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
    }
    
    func initialSetup() {
        
        self.outerProgressView.layer.cornerRadius = 10
        self.outerProgressView.clipsToBounds = true
        
        self.innerProgressView.layer.cornerRadius = 7
        self.innerProgressView.clipsToBounds = true
        
        self.firstOptionView.layer.cornerRadius = 20
        self.firstOptionView.clipsToBounds = true
        
        self.secondOptionView.layer.cornerRadius = 20
        self.secondOptionView.clipsToBounds = true
        
        self.thirdOptionView.layer.cornerRadius = 20
        self.thirdOptionView.clipsToBounds = true
        
        self.forthOptionView.layer.cornerRadius = 20
        self.forthOptionView.clipsToBounds = true
    }
    
    
    @IBAction func btnActionFirstOption(_ sender: Any) {
        self.setUIOfSelectedOption(selectedOption: 1)
    }
    
    @IBAction func btnActionSecondOption(_ sender: Any) {
        self.setUIOfSelectedOption(selectedOption: 2)
    }
    
    @IBAction func btnActionThirdOption(_ sender: Any) {
        self.setUIOfSelectedOption(selectedOption: 3)
    }
    
    @IBAction func btnActionForthOption(_ sender: Any) {
        self.setUIOfSelectedOption(selectedOption: 4)
    }
    
    @IBAction func btnActionNext(_ sender: Any) {
        
    }
    
    @IBAction func btnActionShowResult(_ sender: Any) {
        let vc = ResultsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Helper Function
    //Helper Function
    func setUIOfSelectedOption(selectedOption: Int) {
        if (selectedOption == 1) {
            self.firstOptionView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.secondOptionView.backgroundColor = UIColor .white
            self.thirdOptionView.backgroundColor =  UIColor .white
            self.forthOptionView.backgroundColor =  UIColor .white
            
            self.lblFirstOption.textColor = UIColor .white
            self.lblSecondOption.textColor = UIColor .black
            self.lblThirdOption.textColor = UIColor .black
            self.lblForthOption.textColor = UIColor .black
            
        }
        
        else if (selectedOption == 2) {
            self.firstOptionView.backgroundColor =  UIColor .white
            self.secondOptionView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
            self.thirdOptionView.backgroundColor =  UIColor .white
            self.forthOptionView.backgroundColor =  UIColor .white
            
            self.lblFirstOption.textColor = UIColor .black
            self.lblSecondOption.textColor = UIColor .white
            self.lblThirdOption.textColor = UIColor .black
            self.lblForthOption.textColor = UIColor .black
        }
        
        else if (selectedOption == 3) {
            self.firstOptionView.backgroundColor =  UIColor .white
            self.secondOptionView.backgroundColor = UIColor .white
            self.thirdOptionView.backgroundColor =  UIColor.init(rgb: THEME_COLOR)
            self.forthOptionView.backgroundColor =  UIColor .white
            
            self.lblFirstOption.textColor = UIColor .black
            self.lblSecondOption.textColor = UIColor .black
            self.lblThirdOption.textColor = UIColor .white
            self.lblForthOption.textColor = UIColor .black
        }
        
        else {
            self.firstOptionView.backgroundColor =  UIColor .white
            self.secondOptionView.backgroundColor = UIColor .white
            self.thirdOptionView.backgroundColor =  UIColor .white
            self.forthOptionView.backgroundColor =  UIColor.init(rgb: THEME_COLOR)
            
            self.lblFirstOption.textColor = UIColor .black
            self.lblSecondOption.textColor = UIColor .black
            self.lblThirdOption.textColor = UIColor .black
            self.lblForthOption.textColor = UIColor .white
        }
        
    }
    
}
