//
//  ForgotPasswordViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 18/12/2020.
//

import UIKit

extension ForgotPasswordViewController : AlertViewDelegate  {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

extension ForgotPasswordViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.emailLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        return true
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        self.emailLineView.backgroundColor = UIColor.darkGray
        return true
        
    }
    
}

class ForgotPasswordViewController: UIViewController {

    class func instantiateFromStoryboard() -> ForgotPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotPasswordViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var emailLineView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        
    }
    
    func initialSetup() {
        
        self.txtEmail.delegate = self
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
    }

    @IBAction func backAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender : UIButton ) {
        
        self.view.endEditing(true)
                
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , type:WARNING)
            return
        }
        
//        self.startLoading(message: "")
        
    }

    
}
