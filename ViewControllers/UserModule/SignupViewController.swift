//
//  SignupViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 18/12/2020.
//

import UIKit
import DropDown

extension SignupViewController : AlertViewDelegate  {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

extension SignupViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.nameLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 2 {
            self.emailLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else if textField.tag == 3 {
            self.passwordLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        
        self.userTypeLineView.backgroundColor = UIColor.darkGray
        return true
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.nameLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 2 {
            self.emailLineView.backgroundColor = UIColor.darkGray
        }
        else if textField.tag == 3 {
            self.passwordLineView.backgroundColor = UIColor.darkGray
        }
        
        return true
        
    }
    
}

class SignupViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SignupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignupViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var lblUserType:UITextField!
    
    @IBOutlet weak var nameLineView:UIView!
    @IBOutlet weak var emailLineView:UIView!
    @IBOutlet weak var passwordLineView:UIView!
    @IBOutlet weak var userTypeLineView:UIView!
    
    @IBOutlet weak var imgEye:UIImageView!
    var showPassword = true
    
    // Dropdown
    var userTypeDropDown = DropDown()
    var userTypeArray:[String] = ["Joey" , "Hub"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        // User Type DropDown
        self.userTypeDropDown.anchorView = self.lblUserType // UIView or UIBarButtonItem
        self.userTypeDropDown.bottomOffset = CGPoint(x: 0 , y: (self.userTypeDropDown.anchorView?.plainView.bounds.height)!)
        self.userTypeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblUserType.text = item
            self.userTypeLineView.backgroundColor = UIColor.darkGray
        }
        
    }
    
    func initialSetup() {
        
        self.txtName.delegate = self
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.txtName.attributedPlaceholder = NSAttributedString(string: "Full Name", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.lblUserType.attributedPlaceholder = NSAttributedString(string: "Select User Type", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
    }
    
    @IBAction func showPasswordAction(_ sender : UIButton) {
        
        if showPassword == true {
            self.imgEye.image = UIImage(named: "password_show")
            self.txtPassword.isSecureTextEntry = false
        }
        else {
            self.imgEye.image = UIImage(named: "password_hide")
            self.txtPassword.isSecureTextEntry = true
        }
        
        showPassword = !showPassword
        
    }
    
    @IBAction func loginAction(_ sender : UIButton ) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func userTypeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.userTypeDropDown.dataSource = self.userTypeArray
        self.userTypeLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        self.userTypeDropDown.show()
    }
    
    
    @IBAction func signupAction(_ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        let name = self.txtName.text!
        let password = self.txtPassword.text!
        let userType = self.lblUserType.text!
                
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter full name" , type:WARNING)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , type:WARNING)
            return
        }

        if password.isEmptyOrWhitespace() || password.count < 6 {
            self.showBanner(title: "Alert", subTitle: "Please enter six digit password" , type:WARNING)
            return
        }
        
        if userType.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please select user type" , type:WARNING)
            return
        }
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        self.startLoading(message: "")
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
            
            self.stopLoading()
            
            self.showBanner(title: "Welcome", subTitle: "Your account has been created Successfully" , type: SUCCESS)
            
            self.navigationController?.popViewController(animated: true)
            
            
        })
        
    }
    
    
}
