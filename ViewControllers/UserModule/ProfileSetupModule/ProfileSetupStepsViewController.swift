//
//  ProfileSetupStepsViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 03/05/2021.
//

import UIKit

class ProfileSetupStepsViewController: UIViewController {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnGoToPersonalDetails: UIButton!
    
    class func instantiateFromStoryboard() -> ProfileSetupStepsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ProfileSetupStepsViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.bottomView.layer.cornerRadius = 10
//        self.bottomView.clipsToBounds = true
        
    }
    
    @IBAction func BtnActionGoToPersonalDetails(_ sender: Any) {
        let vc = PersonalDetailViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}


