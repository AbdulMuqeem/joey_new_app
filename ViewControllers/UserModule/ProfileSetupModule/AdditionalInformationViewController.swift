//
//  AdditionalInformationViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 10/05/2021.
//

import UIKit

extension AdditionalInformationViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
//            self.firstNameBorderView.layer.borderColor = UIColor.init(named: THEME_COLOR) as! CGColor
        }
        else if textField.tag == 2  {
//            self.lastNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 3  {
//            self.emailBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 4  {
//            self.phoneNumberBorderLineView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 5  {
//            self.addressBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
//            self.firstNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 2 {
//            self.lastNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 3 {
//            self.emailBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 4 {
//            self.phoneNumberBorderLineView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 5 {
//            self.addressBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        
        return true
    }
    
}

class AdditionalInformationViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> AdditionalInformationViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AdditionalInformationViewController
    }
    
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var bottomButtonView:UIView!
    @IBOutlet weak var heightTopViewConstraint:BaseLayoutConstraint!
    @IBOutlet weak var heightBottomViewConstraint:BaseLayoutConstraint!
    
    @IBOutlet weak var topOrangeView: UIView!
    
    //txtLine outlets
    @IBOutlet weak var institutionNumberView: MDTextField!
    @IBOutlet weak var branchNumberView: MDTextField!
    @IBOutlet weak var accountNumberView: MDTextField!
    @IBOutlet weak var hstNumberView: MDTextField!
    
    @IBOutlet weak var feedbackView: UIView!
    
    @IBOutlet weak var btnSaveAndContinue: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    @IBOutlet weak var shadowView: UIView!

    //BottomView Outlets
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var lblTwitter: UILabel!
    @IBOutlet weak var lblInstagram: UILabel!
    @IBOutlet weak var lblLinkedin: UILabel!
    
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var twitterView: UIView!
    @IBOutlet weak var instagramView: UIView!
    @IBOutlet weak var linkedinView: UIView!
    
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var txtOther:UITextField!

    var isProfile:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
        
    }
    
    func initialSetup() {
    
        self.txtOther.attributedPlaceholder = NSAttributedString(string: "Other", attributes: [
            .foregroundColor: UIColor.darkGray ,
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(14.0))!
        ])
        
        self.institutionNumberView.setupTextField(placeholder: "Institution Number", keyboardType: .asciiCapable)
        self.branchNumberView.setupTextField(placeholder: "Branch Number", keyboardType: .asciiCapable)
        self.accountNumberView.setupTextField(placeholder: "Account Number", keyboardType: .asciiCapable)
        self.hstNumberView.setupTextField(placeholder: "HST Number", keyboardType: .asciiCapable)
        
        if self.isProfile == true {
            
            self.title = "Additional Information"
            self.navigationController?.ShowNavigationBar()
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
            self.navigationController?.ThemedNavigationBar()
            self.navigationController?.ChangeTitleFont()
            
            self.topView.isHidden = true
            self.bottomButtonView.isHidden = true
            
            self.heightTopViewConstraint.constant = 0.0
            self.heightBottomViewConstraint.constant = 0.0
            
        }

        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.topOrangeView.layer.cornerRadius = 10
        self.topOrangeView.clipsToBounds = true
        
        self.feedbackView.layer.cornerRadius = 10
        self.feedbackView.clipsToBounds = true
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
    }

    
    @IBAction func btnActionFeedback(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
    }
    
    @IBAction func BtnActionDone(_ sender: Any) {
        
    }
    
    @IBAction func btnActionSaveAndContinue(_ sender: Any) {
        let vc = CompletedProfileSetupViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActionPrevious(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
