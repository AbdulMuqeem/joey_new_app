//
//  MyAccountViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 11/05/2021.
//

import UIKit

class MyAccountViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> MyAccountViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyAccountViewController
    }

    @IBOutlet weak var topOrangeView: UIView!

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var userImgVw: UIImageView!
    
    @IBOutlet weak var myAccountView: UIView!
    @IBOutlet weak var vehicleInfoView: UIView!
    @IBOutlet weak var workPreferrenceView: UIView!
    @IBOutlet weak var additionalInfoView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func initialSetup() {
        
        self.topOrangeView.layer.cornerRadius = 10
        self.topOrangeView.clipsToBounds = true
        
        self.myAccountView.layer.cornerRadius = 10
        self.myAccountView.clipsToBounds = true
        
        self.vehicleInfoView.layer.cornerRadius = 10
        self.vehicleInfoView.clipsToBounds = true
        
        self.workPreferrenceView.layer.cornerRadius = 10
        self.workPreferrenceView.clipsToBounds = true
        
        self.additionalInfoView.layer.cornerRadius = 10
        self.additionalInfoView.clipsToBounds = true
        
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        
    }
    
    @IBAction func btnActionEditMyAccount(_ sender: Any) {
        
    }
    
    @IBAction func btnActionEditVehicleInfo(_ sender: Any) {
        
    }
    
    @IBAction func btnActionEditWorkPreferrence(_ sender: Any) {
        
    }
    
    @IBAction func btnActionEditAdditionalInfo(_ sender: Any) {
        
    }
    
}
