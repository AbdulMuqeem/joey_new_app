//
//  PersonalDetailViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 03/05/2021.
//

import UIKit


extension PersonalDetailViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
//            self.firstNameBorderView.layer.borderColor = UIColor.init(named: THEME_COLOR) as! CGColor
        }
        else if textField.tag == 2  {
//            self.lastNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 3  {
//            self.emailBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 4  {
//            self.phoneNumberBorderLineView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 5  {
//            self.addressBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 6  {
//            self.unitNumberBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
//            self.firstNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 2 {
//            self.lastNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 3 {
//            self.emailBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 4 {
//            self.phoneNumberBorderLineView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 5 {
//            self.addressBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 6 {
//            self.unitNumberBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        
        return true
    }
    
}

class PersonalDetailViewController: UIViewController {
    
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var bottomButtonView:UIView!
    @IBOutlet weak var heightTopViewConstraint:BaseLayoutConstraint!
    @IBOutlet weak var heightBottomViewConstraint:BaseLayoutConstraint!
    
    //txtLine outlets
    @IBOutlet weak var firstNameBorderView: MDTextField!
    @IBOutlet weak var lastNameBorderView: MDTextField!
    @IBOutlet weak var emailBorderView: MDTextField!
    @IBOutlet weak var phoneNumberBorderLineView: MDTextField!
    @IBOutlet weak var addressBorderView: MDTextField!
    @IBOutlet weak var unitNumberBorderView: MDTextField!
    
    @IBOutlet weak var topOrangeView: UIView!
    
    @IBOutlet weak var btnSaveAndContinue: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    var isProfile:Bool = false
        
    class func instantiateFromStoryboard() -> PersonalDetailViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PersonalDetailViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()        
        self.initialSetup()
    }
    
    func initialSetup() {
        
        self.topOrangeView.layer.cornerRadius = 10
        self.topOrangeView.clipsToBounds = true
        
        self.firstNameBorderView.setupTextField(placeholder: "First Name", keyboardType: .asciiCapable)
        self.lastNameBorderView.setupTextField(placeholder: "Last Name", keyboardType: .asciiCapable)
        self.emailBorderView.setupTextField(placeholder: "Email", keyboardType: .emailAddress)
        self.phoneNumberBorderLineView.setupTextField(placeholder: "Phone Number", keyboardType: .numberPad)
        self.addressBorderView.setupTextField(placeholder: "Address", keyboardType: .asciiCapable)
        self.unitNumberBorderView.setupTextField(placeholder: "Unit Number", keyboardType: .asciiCapable)
        
        if self.isProfile == true {
            
            self.title = "Personal Details"
            self.navigationController?.ShowNavigationBar()
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
            self.navigationController?.ThemedNavigationBar()
            self.navigationController?.ChangeTitleFont()
            
            self.topView.isHidden = true
            self.bottomButtonView.isHidden = true
            
            self.heightTopViewConstraint.constant = 0.0
            self.heightBottomViewConstraint.constant = 0.0
            
        }
        
    }
 
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionSaveAndContinue(_ sender: Any) {
        let vc = VehicleInformationViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActionPrevious(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
