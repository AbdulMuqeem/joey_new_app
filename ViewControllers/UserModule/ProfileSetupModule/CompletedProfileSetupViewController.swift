//
//  CompletedProfileSetupViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 10/05/2021.
//

import UIKit


class CompletedProfileSetupViewController: UIViewController {

    class func instantiateFromStoryboard() -> CompletedProfileSetupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CompletedProfileSetupViewController
    }
    
    @IBOutlet weak var bottomView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.bottomView.layer.cornerRadius = 10
//        self.bottomView.clipsToBounds = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) { // Change `5.0` to the desired number of seconds.
            self.navigateToNextViewController()
        }
        
    }
    
    @IBAction func btnActionContactSupport(_ sender: Any) {
//        self.navigateToNextViewController()
    }
 
    func navigateToNextViewController() {
        let vc = DocumentDetailsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
