//
//  VehicleInformationViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 09/05/2021.
//

import UIKit

extension VehicleInformationViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
//            self.firstNameBorderView.layer.borderColor = UIColor.init(named: THEME_COLOR) as! CGColor
        }
        else if textField.tag == 2  {
//            self.lastNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 3  {
//            self.emailBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        else if textField.tag == 4  {
//            self.phoneNumberBorderLineView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR as! CGColor)] as! CGColor)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
//            self.firstNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 2 {
//            self.lastNameBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 3 {
//            self.emailBorderView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        else if textField.tag == 4 {
//            self.phoneNumberBorderLineView.layer.borderColor = ([UIColor.init(cgColor: THEME_COLOR_GREY as! CGColor)] as! CGColor)
        }
        
        return true
    }
    
}

class VehicleInformationViewController: UIViewController {
    
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var heightTopViewConstraint:BaseLayoutConstraint!
    
    @IBOutlet weak var topOrangeView: UIView!
    
    @IBOutlet weak var bicycleView: UIView!
    @IBOutlet weak var suvView: UIView!
    @IBOutlet weak var carView: UIView!
    @IBOutlet weak var truckView: UIView!
    @IBOutlet weak var scooterView: UIView!
    @IBOutlet weak var vanView: UIView!
    
    @IBOutlet weak var lblBicycle: UILabel!
    @IBOutlet weak var lblSUV: UILabel!
    @IBOutlet weak var lblCar: UILabel!
    @IBOutlet weak var lblTruck: UILabel!
    @IBOutlet weak var lblScooter: UILabel!
    @IBOutlet weak var lblVAN: UILabel!
    
    @IBOutlet weak var bicycleImgVw: UIImageView!
    @IBOutlet weak var suvImgVw: UIImageView!
    @IBOutlet weak var carImgVw: UIImageView!
    @IBOutlet weak var truckImgVw: UIImageView!
    @IBOutlet weak var scooterImgVw: UIImageView!
    @IBOutlet weak var vanImgVw: UIImageView!
    
    @IBOutlet weak var makeBorderView: MDTextField!
    @IBOutlet weak var colorBorderView: MDTextField!
    @IBOutlet weak var modelBorderView: MDTextField!
    @IBOutlet weak var licensePlateBorderView: MDTextField!
    
    @IBOutlet weak var btnSaveAndContinue: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    var isProfile:Bool = false
    let color = UIColor.init(named: "Theme_Orange_Color")
    let labelColor = UIColor.init(named: "Secondary_Label_Color")
   
    class func instantiateFromStoryboard() -> VehicleInformationViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! VehicleInformationViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }

    func initialSetup() {
        
        self.topOrangeView.layer.cornerRadius = 10
        self.topOrangeView.clipsToBounds = true
        
        self.makeBorderView.setupTextField(placeholder: "Make", keyboardType: .asciiCapable)
        self.colorBorderView.setupTextField(placeholder: "Color", keyboardType: .asciiCapable)
        self.modelBorderView.setupTextField(placeholder: "Model", keyboardType: .asciiCapable)
        self.licensePlateBorderView.setupTextField(placeholder: "License Plate", keyboardType: .asciiCapable)
        
        if self.isProfile == true {
            
            self.title = "Vehicle Details"
            self.navigationController?.ShowNavigationBar()
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
            self.navigationController?.ThemedNavigationBar()
            self.navigationController?.ChangeTitleFont()
            
            self.topView.isHidden = true
            self.heightTopViewConstraint.constant = 0.0
            
            self.btnPrevious.isHidden = true
            self.btnSaveAndContinue.isHidden = true
            
        }

    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func bicycleView(_ value: Float, borderColor: UIColor, textColor: UIColor, imageTintColor: UIColor) {
        self.bicycleView.BorderWidth = CGFloat(value)
        self.bicycleView.BorderColor = borderColor
        self.lblBicycle.textColor = textColor
        self.bicycleImgVw.tintColor = imageTintColor
    }
    
    func suvView(_ value: Float, borderColor: UIColor, textColor: UIColor, imageTintColor: UIColor) {
        self.suvView.BorderWidth = CGFloat(value)
        self.suvView.BorderColor = borderColor
        self.lblSUV.textColor = textColor
        self.suvImgVw.tintColor = imageTintColor
    }
    
    func carView(_ value: Float, borderColor: UIColor, textColor: UIColor, imageTintColor: UIColor) {
        self.carView.BorderWidth = CGFloat(value)
        self.carView.BorderColor = borderColor
        self.lblCar.textColor = textColor
        self.carImgVw.tintColor = imageTintColor
    }
    
    func truckView(_ value: Float, borderColor: UIColor, textColor: UIColor, imageTintColor: UIColor) {
        self.truckView.BorderWidth = CGFloat(value)
        self.truckView.BorderColor = borderColor
        self.lblTruck.textColor = textColor
        self.truckImgVw.tintColor = imageTintColor
    }
    
    func scooterView(_ value: Float, borderColor: UIColor, textColor: UIColor, imageTintColor: UIColor) {
        self.scooterView.BorderWidth = CGFloat(value)
        self.scooterView.BorderColor = borderColor
        self.lblScooter.textColor = textColor
        self.scooterImgVw.tintColor = imageTintColor
    }
    
    func vanView(_ value: Float, borderColor: UIColor, textColor: UIColor, imageTintColor: UIColor) {
        self.vanView.BorderWidth = CGFloat(value)
        self.vanView.BorderColor = borderColor
        self.lblVAN.textColor = textColor
        self.vanImgVw.tintColor = imageTintColor
    }
    
    @IBAction func btnActionBicycle(_ sender: Any) {
        self.bicycleView(0.5, borderColor: color ?? UIColor.orange, textColor: color ?? UIColor.orange, imageTintColor: color ?? UIColor.orange)
        self.suvView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.carView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.truckView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.scooterView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.vanView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
    }
    
    @IBAction func btnActionSUV(_ sender: Any) {
        self.suvView(0.5, borderColor: color ?? UIColor.orange, textColor: color ?? UIColor.orange, imageTintColor: color ?? UIColor.orange)
        self.bicycleView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.carView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.truckView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.scooterView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.vanView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
    }
    
    @IBAction func btnActionCar(_ sender: Any) {
        self.carView(0.5, borderColor: color ?? UIColor.orange, textColor: color ?? UIColor.orange, imageTintColor: color ?? UIColor.orange)
        self.bicycleView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.suvView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.truckView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.scooterView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.vanView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
    }
    
    @IBAction func btnActionTruck(_ sender: Any) {
        self.truckView(0.5, borderColor: color ?? UIColor.orange, textColor: color ?? UIColor.orange, imageTintColor: color ?? UIColor.orange)
        self.bicycleView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.suvView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.carView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.scooterView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.vanView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
    }
    
    @IBAction func btnActionScooter(_ sender: Any) {
        self.scooterView(0.5, borderColor: color ?? UIColor.orange, textColor: color ?? UIColor.orange, imageTintColor: color ?? UIColor.orange)
        self.bicycleView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.suvView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.carView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.truckView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.vanView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
    }
    
    @IBAction func btnActionVAN(_ sender: Any) {
        self.vanView(0.5, borderColor: color ?? UIColor.orange, textColor: color ?? UIColor.orange, imageTintColor: color ?? UIColor.orange)
        self.bicycleView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.suvView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.carView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.truckView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
        self.scooterView(0, borderColor: UIColor.clear, textColor: labelColor ?? UIColor.darkGray, imageTintColor: UIColor.darkGray)
    }
    
    @IBAction func btnActionSaveAndContinue(_ sender: Any) {
        let vc = WorkPreferencesViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnActionPrevious(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
