//
//  WorkPreferencesViewController.swift
//  JoeyCo
//
//  Created by MacBook Air on 10/05/2021.
//

import UIKit
import ActionSheetPicker_3_0

class WorkPreferencesViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> WorkPreferencesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WorkPreferencesViewController
    }
    
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var heightTopViewConstraint:BaseLayoutConstraint!
    
    @IBOutlet weak var topOrangeView: UIView!

    @IBOutlet weak var fullTimeView: UIView!
    @IBOutlet weak var partTimeView: UIView!
    @IBOutlet weak var seasonalView: UIView!
    @IBOutlet weak var casualView: UIView!
    
    @IBOutlet weak var lblFullTime: UILabel!
    @IBOutlet weak var lblPartTime: UILabel!
    @IBOutlet weak var lblSeasonal: UILabel!
    @IBOutlet weak var lblCasual: UILabel!
    
    @IBOutlet weak var bestTimeView: UIView!
    @IBOutlet weak var preferredZoneView: UIView!
    
    @IBOutlet weak var lblDayTime: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblPreferredZone: UILabel!
    
    @IBOutlet weak var groceryView: UIView!
    @IBOutlet weak var ecommerceView: UIView!
    
    @IBOutlet weak var lblGrocery: UILabel!
    @IBOutlet weak var lblEcommerce: UILabel!
    
    @IBOutlet weak var btnSaveAndContinue: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    @IBOutlet weak var shadowView: UIView!
    
    //BottomView Outlets
    @IBOutlet weak var bottomView:UIView!
    
    @IBOutlet weak var calgaryView: UIView!
    @IBOutlet weak var ottawaView: UIView!
    @IBOutlet weak var jeddaView: UIView!
    @IBOutlet weak var downtownTorontoView: UIView!
    @IBOutlet weak var montrealView: UIView!
    @IBOutlet weak var helifaxView: UIView!
    
    @IBOutlet weak var lblCalgary: UILabel!
    @IBOutlet weak var lblOttawa: UILabel!
    @IBOutlet weak var lblJedda: UILabel!
    @IBOutlet weak var lblDowntownToronto: UILabel!
    @IBOutlet weak var lblMontreal: UILabel!
    @IBOutlet weak var lblHelifax: UILabel!
    
    var isProfile:Bool = false
    let color = UIColor.init(named: "Theme_Orange_Color")
    let labelColor = UIColor.init(named: "Secondary_Label_Color")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialSetup()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.shadowView.addGestureRecognizer(tap)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.bottomView.clipsToBounds = true
        self.bottomView.layer.cornerRadius = 60
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.view.layoutIfNeeded()
    }
    
    func initialSetup() {
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.topOrangeView.layer.cornerRadius = 10
        self.shadowView.clipsToBounds = true
        
        if self.isProfile == true {
            
            self.title = "Work Preference"
            self.navigationController?.ShowNavigationBar()
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
            self.navigationController?.ThemedNavigationBar()
            self.navigationController?.ChangeTitleFont()
            
            self.topView.isHidden = true
            self.heightTopViewConstraint.constant = 0.0
            
            self.btnPrevious.isHidden = true
            self.btnSaveAndContinue.isHidden = true
            
        }
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.bottomView.isHidden = true
        self.shadowView.isHidden = true
    }

    func fullTime(_ value: Float, color: UIColor, textColor: UIColor) {
        self.fullTimeView.BorderWidth = CGFloat(value)
        self.fullTimeView.BorderColor = color
        self.lblFullTime.textColor = textColor
    }
    
    func partTime(_ value: Float, color: UIColor, textColor: UIColor) {
        self.partTimeView.BorderWidth = CGFloat(value)
        self.partTimeView.BorderColor = color
        self.lblPartTime.textColor = textColor
    }
    
    func seasonal(_ value: Float, color: UIColor, textColor: UIColor) {
        self.seasonalView.BorderWidth = CGFloat(value)
        self.seasonalView.BorderColor = color
        self.lblSeasonal.textColor = textColor
    }
    
    func casual(_ value: Float, color: UIColor, textColor: UIColor) {
        self.casualView.BorderWidth = CGFloat(value)
        self.casualView.BorderColor = color
        self.lblCasual.textColor = textColor
    }
    
    func grocery(_ value: Float, color: UIColor, textColor: UIColor) {
        self.groceryView.BorderWidth = CGFloat(value)
        self.groceryView.BorderColor = color
        self.lblGrocery.textColor = textColor
    }
    
    func ecommerce(_ value: Float, color: UIColor, textColor: UIColor) {
        self.ecommerceView.BorderWidth = CGFloat(value)
        self.ecommerceView.BorderColor = color
        self.lblEcommerce.textColor = textColor
    }
    
    func calgary(_ value: Float, color: UIColor, textColor: UIColor) {
        self.calgaryView.BorderWidth = CGFloat(value)
        self.calgaryView.BorderColor = color
        self.lblCalgary.textColor = textColor
    }
    
    func ottawa(_ value: Float, color: UIColor, textColor: UIColor) {
        self.ottawaView.BorderWidth = CGFloat(value)
        self.ottawaView.BorderColor = color
        self.lblOttawa.textColor = textColor
    }
    
    func jedda(_ value: Float, color: UIColor, textColor: UIColor) {
        self.jeddaView.BorderWidth = CGFloat(value)
        self.jeddaView.BorderColor = color
        self.lblJedda.textColor = textColor
    }
    
    func toronto(_ value: Float, color: UIColor, textColor: UIColor) {
        self.downtownTorontoView.BorderWidth = CGFloat(value)
        self.downtownTorontoView.BorderColor = color
        self.lblDowntownToronto.textColor = textColor
    }
    
    func montreal(_ value: Float, color: UIColor, textColor: UIColor) {
        self.montrealView.BorderWidth = CGFloat(value)
        self.montrealView.BorderColor = color
        self.lblMontreal.textColor = textColor
    }
    
    func helifax(_ value: Float, color: UIColor, textColor: UIColor) {
        self.helifaxView.BorderWidth = CGFloat(value)
        self.helifaxView.BorderColor = color
        self.lblHelifax.textColor = textColor
    }
    
    @IBAction func btnActionFullTime(_ sender: Any) {
        self.fullTime(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.partTime(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.seasonal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.casual(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionPartTime(_ sender: Any) {
        self.partTime(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.fullTime(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.seasonal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.casual(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionSeasonal(_ sender: Any) {
        self.seasonal(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.fullTime(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.partTime(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.casual(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionCasual(_ sender: Any) {
        self.casual(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.fullTime(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.partTime(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.seasonal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionBestTime(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Select Time", datePickerMode: .date , selectedDate: Date(), minimumDate: nil , maximumDate: Date() , doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.lblDayTime.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func btnActionPreferredZone(_ sender: Any) {
        self.bottomView.isHidden = false
        self.shadowView.isHidden = false
    }
    
    @IBAction func btnActionGrocery(_ sender: Any) {
        self.grocery(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.ecommerce(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionEcommerce(_ sender: Any) {
        self.ecommerce(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.grocery(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionSaveAndContinue(_ sender: Any) {
        let vc = AdditionalInformationViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActionPrevious(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //BottomView Actions:
    @IBAction func btnActionCalgary(_ sender: Any) {
        self.calgary(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.ottawa(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.jedda(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.toronto(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.montreal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.helifax(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionOttawa(_ sender: Any) {
        self.calgary(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.ottawa(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.jedda(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.toronto(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.montreal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.helifax(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionJedda(_ sender: Any) {
        self.calgary(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.ottawa(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.jedda(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.toronto(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.montreal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.helifax(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionDowntownToronto(_ sender: Any) {
        self.calgary(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.ottawa(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.jedda(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.toronto(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.montreal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.helifax(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionMontreal(_ sender: Any) {
        self.calgary(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.ottawa(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.jedda(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.toronto(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.montreal(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
        self.helifax(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
    }
    
    @IBAction func btnActionHelifax(_ sender: Any) {
        self.calgary(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.ottawa(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.jedda(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.toronto(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.montreal(0, color: UIColor.clear, textColor: labelColor ?? UIColor.darkGray)
        self.helifax(0.5, color: UIColor.orange, textColor: color ?? UIColor.orange)
    }
    
}

