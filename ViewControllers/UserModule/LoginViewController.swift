//
//  LoginViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 16/12/2020.
//

import UIKit

extension LoginViewController : AlertViewDelegate  {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

extension LoginViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.emailLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        else {
            self.passwordLineView.backgroundColor = UIColor.init(rgb: THEME_COLOR)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 1 {
            self.emailLineView.backgroundColor = UIColor.darkGray
        }
        else {
            self.passwordLineView.backgroundColor = UIColor.darkGray
        }
        
        return true
    }
    
}


class LoginViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    
    @IBOutlet weak var emailLineView:UIView!
    @IBOutlet weak var passwordLineView:UIView!
    
    @IBOutlet weak var imgEye:UIImageView!
    var showPassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
        self.DarkStatusBar()
        
        //added for testing
        self.txtEmail.text = "abdul@gmail.com"
        self.txtPassword.text = "Abdul@123"
        
    }
    
    func initialSetup() {
        
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
        self.txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [
            .foregroundColor: UIColor.init(rgb: 0xB1B1B1),
            .font: UIFont.init(name: "Poppins-Medium", size: GISTUtility.convertToRatio(12.0))!
        ])
        
    }
    
    @IBAction func showPasswordAction(_ sender : UIButton) {
        
        if showPassword == true {
            self.imgEye.image = UIImage(named: "password_show")
            self.txtPassword.isSecureTextEntry = false
        }
        else {
            self.imgEye.image = UIImage(named: "password_hide")
            self.txtPassword.isSecureTextEntry = true
        }
        
        showPassword = !showPassword
        
    }
    
    @IBAction func forgotPasswordAction(_ sender : UIButton ) {
        let vc = ForgotPasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signupAction(_ sender : UIButton ) {
        let vc = SignupViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func loginAction(_ sender : UIButton ) {
        
        self.view.endEditing(true)
        
        let password = self.txtPassword.text!
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , type:WARNING)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , type:WARNING)
            return
        }
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection", type:FAILURE)
            self.stopLoading()
            return
        }
        
        self.startLoading(message: "")
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
            
//            if self.email == email && self.password == password {
                self.stopLoading()
                
                let vc = ProfileSetupStepsViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                
//                let name = "Abdul Muqeem"
//                self.showBanner(title: "Welcome, \(name)!", subTitle: "You have been Login Successfully" , type: SUCCESS)
                
//            }
//            else {
//                self.stopLoading()
//                self.showBanner(title: "Verification Failed Error", subTitle: "Invalid login credentials" , type: FAILURE)
//            }
            
        })
    }
    
}
