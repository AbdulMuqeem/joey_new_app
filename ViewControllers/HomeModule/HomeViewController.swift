//
//  HomeViewController.swift
//  JoeyCo
//
//  Created by Muqeem's Macbook on 16/12/2020.
//

import UIKit
import GoogleMaps

extension HomeViewController: AlertViewDelegate , AlertViewDelegateAction {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func okButtonAction() {
        
        if self.isLogout == true {
            let nav = RootViewController.instantiateFromStoryboard()
            
            if #available(iOS 13.0, *) {
                SceneDelegate.getInstatnce().window?.rootViewController = nav
            } else {
                AppDelegate.getInstatnce().window?.rootViewController = nav
            }
            
            let VC = LoginViewController.instantiateFromStoryboard()
            nav.pushViewController(VC, animated: true)
        }
        else {
            
            let url = URL(string: UIApplication.openSettingsURLString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            self.shadowView.isHidden = false
            self.bottomView.setView(view: self.bottomView, hidden: false)

        }
        
        self.alertView.isHidden = true
    }
    
    func cancelAction() {
        self.alertView.isHidden = true
        self.offlineView.isHidden = false
        self.onlineView.isHidden = true
        self.bottomView.isHidden = true
    }
    
}

extension HomeViewController : CLLocationManagerDelegate , GMSMapViewDelegate {
    
    func determineMyCurrentLocation() {
        
        locationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc = manager.location?.coordinate
        latitude = (loc?.latitude)!
        longitude = (loc?.longitude)!
        
        print("Latitude: \(latitude!) & Longitude: \(longitude!)")
        self.getMap(latitude!, longitude!)
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func getMap(_ latitude:Double, _ longitude:Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude , longitude: longitude , zoom: 16)
        self.mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        // marker.title = self.SellerLocation
        marker.map = mapView
        
    }
    
    func didAccesToLocation() -> Bool {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
            print("Not determine your current location")
            return true
        }
        else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse {
            print("access to location")
            return true
        }
        else {
            print("not access to location")
            DispatchQueue.main.async {
                self.callpopUp()
            }
            return false
        }
    }
    
    func callpopUp() {
        
        self.alertView.btnOkAction.setTitle("Settings", for: .normal)
        self.alertView.btnCancel.setTitle("Cancel", for: .normal)
        
        self.alertView.alertShow(title: "Access Permission", msg: "Joey App require access to your location, Enable location services for more accurate location", id: 1)
        self.alertView.isHidden = false
        
    }
    
}


class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var onlineView:UIView!
    @IBOutlet weak var offlineView:UIView!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var bottomOfflineView:UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    
    var VehicleNameListArray:[String] = ["Bicycle" , "Scooter" , "Car" , "Truck" , "SUV" , "Van"]
    var VehicleImageListArray:[UIImage] = [UIImage(named:"cycle")! , UIImage(named:"bike")! , UIImage(named:"car_home")! , UIImage(named:"truck_home")! , UIImage(named:"suv_home")! , UIImage(named:"van_home")!]
    
    var locationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    
    var isLogout:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        //Register cell
        let nib = UINib(nibName: "HomeVehicleTypeCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "HomeVehicleTypeCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.HideNavigationBar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.onlineView.roundCorners(corners: [.topLeft , .bottomLeft], radius: 20)
        self.offlineView.roundCorners(corners: [.topRight , .bottomRight], radius: 20)
        self.bottomOfflineView.roundCorners(corners: [.topRight , .bottomRight], radius: 20)
        self.bottomView.roundCorners(corners: [.topRight , .topLeft], radius: 60)
        self.view.layoutIfNeeded()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.bottomView {
            self.shadowView.isHidden = true
            self.offlineView.isHidden = false
            self.onlineView.isHidden = true
            self.bottomView.isHidden = true
        }
    }
    
    func initialSetup() {
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        self.alertView.delegateAction = self
        
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
        
        self.DarkStatusBar()
        self.mapView.delegate = self
        //        self.didAccesToLocation()
        //        self.determineMyCurrentLocation()
        
        self.getMap(33.7295, 73.0372)
        
        if self.isLogout == true {
            
            DispatchQueue.main.async {
                
                self.alertView.btnOkAction.setTitle("Logout", for: .normal)
                self.alertView.btnCancel.setTitle("Cancel", for: .normal)
                
                self.alertView.alertShow(title: "Alert", msg: "Are you sure you want to Logout ?", id: 1)
                self.alertView.isHidden = false
                
            }
        }
    }
    
    @IBAction func sideMenuAction(_ sender : UIButton) {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func onlineAction(_ sender : UIButton) {
        self.offlineView.isHidden = true
        self.onlineView.isHidden = false
        self.callpopUp()
    }
    
    @IBAction func offlineAction(_ sender : UIButton) {
        self.offlineView.isHidden = false
        self.onlineView.isHidden = true
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
    }
    
    @IBAction func startWorkAction(_ sender : UIButton) {
        self.shadowView.isHidden = true
        self.bottomView.isHidden = true
    }
    
}

extension HomeViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.VehicleNameListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width //- 20
        let height = GISTUtility.convertToRatio(130.0)
        let width = GISTUtility.convertToRatio(screenWidth/2.7)
        return CGSize(width: width , height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:HomeVehicleTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier:"HomeVehicleTypeCell" , for: indexPath) as! HomeVehicleTypeCell
        
        cell.bgView.clipsToBounds = true
        cell.bgView.layer.cornerRadius = GISTUtility.convertToRatio(60.0)
        cell.bgView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        cell.bgView.layoutIfNeeded()

        cell.lblVehicleName.text = self.VehicleNameListArray[indexPath.row]
        cell.imgVehicle.image = self.VehicleImageListArray[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        let obj = self.MyCourseListArray![indexPath.row]
        //        self.navigationDelegate?.didNavigate(course: obj)
    }
}
